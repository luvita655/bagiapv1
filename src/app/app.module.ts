import { BrowserModule } from '@angular/platform-browser';
import {NgModule, NO_ERRORS_SCHEMA} from '@angular/core';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { AdminComponent } from './layout/admin/admin.component';
import { BreadcrumbsComponent } from './layout/admin/breadcrumbs/breadcrumbs.component';
import { TitleComponent } from './layout/admin/title/title.component';
import { AuthComponent } from './layout/auth/auth.component';
import {SharedModule} from './shared/shared.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { CourseService } from './services/course.service';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { far } from '@fortawesome/free-regular-svg-icons';
import { fab } from '@fortawesome/free-brands-svg-icons';
import { DemoMaterialModule } from './material-module';
import { CommonModule } from '@angular/common';
import { HttpService } from './services/common/http.service';
import { WidgetCustomModule } from './widget-custom/widget-custom.module';
import { Toast } from 'ngx-toastr';

library.add(fas, far, fab);

@NgModule({
  declarations: [
    AppComponent,
    AdminComponent,
    BreadcrumbsComponent,
    TitleComponent,
    AuthComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    SharedModule,
    DemoMaterialModule,
    CommonModule,
    WidgetCustomModule,
  ],
  schemas: [ NO_ERRORS_SCHEMA ],
  providers: [
    CourseService,
    HttpService,
  ],
  bootstrap: [AppComponent],
})
export class AppModule { }
