export const Module = "babuvi";
export const ApiBaseUrl = "http://api.appixi.net/api/v1/";
export const APP_NAME = 'Babuvi';
export const MessageType = {
    Default: 'default',
    Info: 'info',
    Success: 'success',
    Wait: 'wait',
    Error: 'error',
    Warning: 'warning',
};

export const ApDomainType = {
    AllType: '00',
    UserSetting: '01',
    LevelUser: '02',
    UserType: '03',
    ReceiptType: '04',
    PaymentType: '05',
    ReceiptStatus: '06',
    OrderStatus: '07',
    SystemSetting: '08',
    TaskType: '09',
    TaskStatus: '10',
    AssignStatus: '11',
    CartItemStatus: '12',
    PaymentStatus: '13',
    OrderType: '14',
    WalletTransactionsType: '15',
    StatusBasic: '16',
    AmGenCodeType: '17',
    WithdrawalRequestStatus: '18',
    IsDefault: '19',
    FeeType: '20',
    ExchangeRateType: '21',
    ShippingType: '22',
    OrderSource: '23',
    MerchandiseWarehouseStatus: '24',
    ServiceGroupType: '25',
    IsOption: '26',
    Reasion: '27',
    IsAdmin: '28',
    WarehouseLocation: '29',
    UserStatus: '30',
    ControlType: '31',
    ChatRole: '32',
    TransactionStatus: '33',
    AreaType: '34',
    SymbolsLocation: '35',
    ComplainStatus: '36',
    ComplainType: '37',
    MerchandiseType: '38',
    DeliveryRequestType: '39',
    ServiceLocationDisplay: '40',
    WarehouseImpStatus: '41',
    WarehouseImpType: '42',
    DeliveryRequestStatus: '43',
    AttachFileType: '44',
    WarehouseExpStatus: '45',
    WarehouseExpType: '46',
    CompanyProfile: '47',
    ShipmentStatus: '48',
    ShipmentType: '49',
    WarehouseExpDetailStatus: '50',
}

export const paging = {
    perPage: 15,
    page: 1
};

export const usersetting = {
    Kho_Nhan_Hang: "1",
    Level: "2",
    CareStaff: "3",
    OrderStaff: "4",
    DeliveryAddressDefault: "5",
    Ty_Le_Quy_Doi_Can_Nang: "6"
    
}

export const ShiftStatus = {
    STATUS_APPROVED: 1,
    STATUS_WAITING_APPROVE: 2,
    STATUS_EDIT_REQUESTED: 3,
    STATUS_DELETE_REQUESTED: 4,
};

export const ReceiptStatus = {
    CHOXACNHAN: 1,
    XACNHAN: 2,
    HUY: 3
}
export const NUMBER_OF_REQUEST_ATTEMPTS = 1; // try request api onetime again

export const PageSize = 20;

export const ApiApplication = {
    login: ApiBaseUrl + 'auth/login',
    course: {
        getCourse: ApiBaseUrl + 'get-course-list',
        delCourse: ApiBaseUrl + 'delete-course',
        itemByCourse: ApiBaseUrl + 'get-items-by-course',
        detailCourse: ApiBaseUrl + 'get-basic-of-course',
        detailGroupWorkout: ApiBaseUrl + 'get-detail-group-workout',
        createCourseItem: ApiBaseUrl + 'create-new-item',
        getZone: ApiBaseUrl + 'get-zone-list',
        updBasicCourse: ApiBaseUrl + 'update-basic-of-course',
        searchDiscoverCourse: ApiBaseUrl + 'get-list-discover-course',
        getDiscoverCourse: ApiBaseUrl + 'get-list-discover-course',
        getLstCourseByKey: ApiBaseUrl + 'get-course-by-keyword',
        createNewCourse: ApiBaseUrl + 'create-new-course',
    },
    group: {
        delGroupItem: ApiBaseUrl + 'delete-group-of-item',
        delExerciseOfGroup: ApiBaseUrl + 'delete-exercise-of-group',
        groupByKeyword: ApiBaseUrl + 'get-group-by-keyword',
        createGroup: ApiBaseUrl + 'create-new-group',
        delGroup: ApiBaseUrl + 'delete-group',
        updGroup: ApiBaseUrl + 'update-group',
        addNewGroup: ApiBaseUrl + 'add-new-group-to-item',
    },
    item: {
        delItem: ApiBaseUrl + 'delete-item',
        updBasicItem: ApiBaseUrl + 'update-basic-item',
    },
    exercise: {
        addNewExerciseToGroup: ApiBaseUrl + 'add-new-exercise-to-group',
        exerciseByKeyword: ApiBaseUrl + 'get-exercise-by-keyword',
        updOrderExercise: ApiBaseUrl + 'update-order-of-exercise',
        getDetailExcercise: ApiBaseUrl + 'get-detail-exercise',
        updExcercise: ApiBaseUrl + 'update-exercise',
        createExcercise: ApiBaseUrl + 'create-new-exercise',
        delExcercise: ApiBaseUrl + 'delete-exercise',
    }
};

export const LocalStorageKey = {
    ACCOUNT_INFO: 'account-info',
}

export const NAVIGATE_ROUTING = {
    COURCE_COMPANY: 'course',
}

export const HttpStatus = {
    CONTINUE: 100,
    SWITCHING_PROTOCOLS: 101,
    PROCESSING: 102,
    OK: 200,
    CREATED: 201,
    ACCEPTED: 202,
    NON_AUTHORITATIVE_INFORMATION: 203,
    NO_CONTENT: 204,
    RESET_CONTENT: 205,
    PARTIAL_CONTENT: 206,
    AMBIGUOUS: 300,
    MOVED_PERMANENTLY: 301,
    FOUND: 302,
    SEE_OTHER: 303,
    NOT_MODIFIED: 304,
    TEMPORARY_REDIRECT: 307,
    PERMANENT_REDIRECT: 308,
    BAD_REQUEST: 400,
    UNAUTHORIZED: 401,
    PAYMENT_REQUIRED: 402,
    FORBIDDEN: 403,
    NOT_FOUND: 404,
    METHOD_NOT_ALLOWED: 405,
    NOT_ACCEPTABLE: 406,
    PROXY_AUTHENTICATION_REQUIRED: 407,
    REQUEST_TIMEOUT: 408,
    CONFLICT: 409,
    GONE: 410,
    LENGTH_REQUIRED: 411,
    PRECONDITION_FAILED: 412,
    PAYLOAD_TOO_LARGE: 413,
    URI_TOO_LONG: 414,
    UNSUPPORTED_MEDIA_TYPE: 415,
    REQUESTED_RANGE_NOT_SATISFIABLE: 416,
    EXPECTATION_FAILED: 417,
    I_AM_A_TEAPOT: 418,
    UNPROCESSABLE_ENTITY: 422,
    TOO_MANY_REQUESTS: 429,
    INTERNAL_SERVER_ERROR: 500,
    NOT_IMPLEMENTED: 501,
    BAD_GATEWAY: 502,
    SERVICE_UNAVAILABLE: 503,
    GATEWAY_TIMEOUT: 504,
    HTTP_VERSION_NOT_SUPPORTED: 505
};

export const NavigateRouting = {
    BackToLogin: "auth",
    NotFound: '404',
    Organization: '/organization',
    AddOrganization: '/organization/add',
    EditOrganization: '/organization/edit',
    Role: '/role',
    RoleManager: '/role/manager',
    AddRole: '/role/add',
    EditRole: '/role/edit',
    Staff: '/staff',
    AddStaff: '/staff/add',
    AddStaffOrganization: 'staff/add/organization',
    EditStaff: '/staff/edit',
    API_Key: '/api-key',
    API_Function: '/api-function',
    Company: '/company',
    AddCompany: '/company/add',
    EditCompany: '/company/edit',
    Blocks: '/blocks',
    Units: '/units',
    STAFF_ROLE: '/staff-role',
    Log: '/log',
    Admin: '/admin',
    Team: '/team',
    Teamkbn: '/teamkbn',
    StaffTeam: '/staff-team',
    AddTeamkbn: '/teamkbn/add',
    EditTeamkbn: '/teamkbn/edit',
    Setting: '/setting',
    PasswordBilling: '/password-billing',
    Forbidden: '/403',
};
