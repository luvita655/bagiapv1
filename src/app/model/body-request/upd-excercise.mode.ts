export class UpdExcerciseModel {
    token: string = null;
    token_type: string = null;
    expries_in: number = null;

    // id: number = null;
    // name: abdominal crunches 1
    // image: (binary)
    // description: Lie on your back with your knees bent and your arms stretched forward. Then lift your upper body off the floor. Hold for a few seconds and slowly return. It primarily works the rectus abdominis muscle and the obliques.
    // video: https://youtu.be/zOZr-pXgawk
    // type: by_time
    // reps: 0
    // time_per_rep: 2.5
    // tts_guide: n/a
    // met: 2.8
    // default_duration: 30
}