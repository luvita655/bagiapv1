import { validate } from './item-exercise.model';

export class DetailCourseModel {
    banner: string = null;
    created_at: string = null;
    description: string = null;
    id: number = null;
    layout_type: string = null;
    name: string = null;
    updated_at: string = null;
    zone_id: number = null;
}

export class validateDetailCourse {
    detect_banner: boolean = false;
    smg_banner: string = null;
    
    detect_description: boolean = false;
    smg_description: string = null;
    
    detect_name: boolean = false;
    smg_name: string = null;
    
    detect_zone: boolean = false;
    smg_zone: string = null;
    
    detect_layout_type: boolean = false;
    smg_layout_type: string = null;
}