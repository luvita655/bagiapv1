import { GroupWorkoutsModel } from './group-workouts.model';

export class DetailItemsCourseModel {
    banner: string = null;
    id: number = null;
    name: string = null;
    group_workouts: GroupWorkoutsModel[];
}