import { DetailItemsCourseModel } from './detail-item-course.model';

export class DiscoverCourseModel {
    id: number = null;
    items: DetailItemsCourseModel[];
    layout_type: string = null;
    name: string = null;
}