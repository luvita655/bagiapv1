import { ItemExerciseModel } from './item-exercise.model';
import { PaginationModel } from './pagination.model';

export class ExcerciseWithPagination {
    exercises: ItemExerciseModel[];
    pagination: PaginationModel;
}