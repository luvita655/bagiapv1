import { ItemExerciseModel } from './item-exercise.model';

export class ExerciseModel {
    banner: string = null;
    description: string = null;
    exercises: ItemExerciseModel[];
    thumb: string = null;
    id: number = null;
    name: string = null;
    total_exercise: number = null;
    total_time: number = null;
}