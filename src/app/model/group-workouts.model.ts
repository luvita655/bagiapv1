import { ExerciseModel } from './exercises.model';

export class GroupWorkoutsModel{
    banner: string = null;
    description: string = null;
    group_id: number = null;
    id: number = null;
    name: string = null;
    thumb: string = null;
    exercises: ExerciseModel[];
    total_exercise: number = null;
    total_time: number = null;
    validate?: validateGroup;
}

export class validateGroup {
    smgName: string = null;
    detectSmgName: boolean = false;

    smgBanner: string = null;
    detectSmgBanner: boolean = false;

    smgThumb: string = null;
    detectSmgThumb: boolean = false;

    smgDescription: string = null;
    detectSmgDescription: boolean = false;
}