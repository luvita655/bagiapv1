import { validateForm } from './rs-account.model';

export class ItemExerciseModel {
    default_duration: string = null;
    description: string = null;
    exercise_group_id?: number = null;
    id: number = null;
    image: string = null;
    thumb_image: string = null;
    met: string = null;
    name: string = null;
    reps: string = null;
    time_per_rep: string = null;
    tts_guide: string = null;
    type: string = null;
    video: string = null;
    valid: validate = new validate();
}

export class validate {
    smg_name: string = null;
    detect_name: boolean = false;

    smg_default_duration: string = null;
    detect_default_duration: boolean = false;

    smg_description: string = null;
    detect_description: boolean = false;

    smg_image: string = null;
    detect_image: boolean = false;

    smg_image_thumb: string = null;
    detect_image_thumb: boolean = false;

    smg_met: string = null;
    detect_met: boolean = false;

    smg_reps: string = null;
    detect_reps: boolean = false;

    smg_time_per_rep: string = null;
    detect_time_per_rep: boolean = false;

    smg_tts_guide: string = null;
    detect_tts_guide: boolean = false;

    smg_type: string = null;
    detect_type: boolean = false;

    smg_video: string = null;
    detect_video: boolean = false;
}