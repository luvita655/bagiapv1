import { DetailItemsCourseModel } from './detail-item-course.model';

export class ItemsCourseModel {
    banner: string = null;
    id: number = null;
    name: string = null;
    items: DetailItemsCourseModel[];
}