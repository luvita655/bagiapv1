export class PaginationModel {
    count: number = null;
    current_page: number = null;
    per_page: number = null;
    total: number = null;
    total_pages: number = null;
}