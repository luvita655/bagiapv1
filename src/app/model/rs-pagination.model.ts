export class RsPaginationModel {
    page: number = null;
    per_page: number = null;
    keyword?: string = null;
}