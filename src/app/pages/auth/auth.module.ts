import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AuthRoutingModule} from './auth-routing.module';
import {SharedModule} from '../../shared/shared.module';
import { HttpService } from '../../services/common/http.service';
import { Toast } from 'ngx-toastr';

@NgModule({
  imports: [
    CommonModule,
    AuthRoutingModule,
    SharedModule
  ],
  declarations: [],
  providers: [
    // HttpService,
  ],
})
export class AuthModule { }
