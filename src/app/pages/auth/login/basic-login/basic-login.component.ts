import { Component, OnInit } from '@angular/core';
import { LoginService } from '../../../../services/login.service';
import { RsAccountModel } from '../../../../model/rs-account.model';
import { AccountModel } from '../../../../model/account.model';
import { LocalStorageKey, NAVIGATE_ROUTING } from '../../../../config/app.config';
import { AlertService } from '../../../../widget-custom/_alert/alert.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { CustomToastComponent } from '../../../../widget-custom/custom-toast.component';

@Component({
  selector: 'app-basic-login',
  templateUrl: './basic-login.component.html',
  styleUrls: ['./basic-login.component.scss']
})
export class BasicLoginComponent implements OnInit {
  
  options = {
      autoClose: false,
      keepAfterRouteChange: false
  };

  username: string;
  password: string;
  rsLogin: RsAccountModel;
  account: AccountModel;
  loading: boolean = false;

  constructor(
    private loginService: LoginService,
    protected alertService: AlertService,
    private _router: Router,
    private toastr: ToastrService,
    ) {
    this.rsLogin = new RsAccountModel();
    this.username = null;
    this.password = null;
  }

  ngOnInit() {
    //
  }

  validate(): boolean {
    if (!this.username.trim()) {
      // this.rsLogin.validate.ckUserName
      return false;
    } else if (!this.password.trim()) {

      return false;
    }

    return true;
  }

  login() {
    this.loading = true;
    this.rsLogin.email = this.username;
    this.rsLogin.password = this.password;
    this.loginService.getLogin(this.rsLogin).subscribe(res => {
      if (res.result.code === 200) {
        this.account = res.result.data
        localStorage.setItem(LocalStorageKey.ACCOUNT_INFO, JSON.stringify(this.account));
        this._router.navigate(['/' + NAVIGATE_ROUTING.COURCE_COMPANY]);
        this.toastSuccess("Đăng nhập thành công");
      } else {
        this.toastErr(res.result.message);
      }
      this.loading = false;
    }, err => {
      this.loading = false;
    });
  }

  toastSuccess(name: string){
    this.toastr.success(name,'', {toastComponent: CustomToastComponent});
  }

  toastErr(name: string){
    this.toastr.error(name,'', {toastComponent: CustomToastComponent});
  }

}
