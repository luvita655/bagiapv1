import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BasicLoginComponent } from './basic-login.component';
import {BasicLoginRoutingModule} from './basic-login-routing.module';
import {SharedModule} from '../../../../shared/shared.module';
import { FormsModule } from '@angular/forms';
import { LoginService } from '../../../../services/login.service';
import { HttpService } from '../../../../services/common/http.service';
import { HttpModule } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { WidgetCustomModule } from '../../../../widget-custom/widget-custom.module';
import { ToastrService } from 'ngx-toastr';

@NgModule({
  imports: [
    CommonModule,
    BasicLoginRoutingModule,
    SharedModule,
    FormsModule,
    HttpModule,
    WidgetCustomModule,
  ],
  declarations: [
    BasicLoginComponent,
  ],
  providers: [
    LoginService,
    HttpService,
    ToastrService
  ],
})
export class BasicLoginModule { }
