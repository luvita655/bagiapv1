import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CourseRoutingModule } from './course-routing.module';
import { DashboardDefaultComponent } from './dashboard-default/dashboard-default.component';
import {SharedModule} from '../../shared/shared.module';
import {ChartModule} from 'angular2-chartjs';
import { CourseService } from '../../services/course.service';
import { HttpService } from '../../services/common/http.service';
import { HttpModule } from '@angular/http';
import { WidgetCustomModule } from '../../widget-custom/widget-custom.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PassDataService } from '../../services/pass-data.services';
import { DetailCourseComponent } from './detail-course/detail-course.component';
import { DetailItemCourseComponent } from './detail-items-course/detail-item-course.component';
import { CollapseModule } from '../../widget-custom/_collapse/collapse.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { DemoMaterialModule } from '../../material-module';
import { ModalModule } from 'ngx-bootstrap/modal';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { detailItemDialogComponent } from './detail-items-course/detail-item-dialog/detail-item-dialog.component';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ItemService } from '../../services/item.service';
import { GroupService } from '../../services/group.service';
import { ExcerciseService } from '../../services/exercise.service';
import { ToastrService } from 'ngx-toastr';
import { EditCourseComponent } from './edit-course/edit-course.component';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { NewGroupDialogComponent } from './detail-items-course/new-group-dialog/new-group-dialog.component';
import { CreateCourseComponent } from './create-course/create-course.component';


@NgModule({
  imports: [
    CourseRoutingModule,
    SharedModule,
    ChartModule,
    HttpModule,
    WidgetCustomModule,
    FontAwesomeModule,
    FormsModule,
    CollapseModule,
    CommonModule,
    DemoMaterialModule,
    ReactiveFormsModule,
    ModalModule.forRoot(),
    PopoverModule.forRoot(),
    TabsModule.forRoot(),
  ],
  declarations: [
    DashboardDefaultComponent,
    DetailCourseComponent,
    DetailItemCourseComponent,
    detailItemDialogComponent,
    EditCourseComponent,
    NewGroupDialogComponent,
    CreateCourseComponent,
  ],
  providers: [
    CourseService,
    HttpService,
    PassDataService,
    { provide: MAT_DIALOG_DATA, useValue: {} },
     { provide: MatDialogRef, useValue: {} },
     ItemService,
     GroupService,
     ExcerciseService,
     ToastrService
  ],
  entryComponents: [
    detailItemDialogComponent,
    NewGroupDialogComponent,
    CreateCourseComponent,
  ],
})
export class CourseModule { }
