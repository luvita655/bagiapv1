import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateCourseComponent } from './create-course/create-course.component';
import { DashboardDefaultComponent } from './dashboard-default/dashboard-default.component';
import { DetailCourseComponent } from './detail-course/detail-course.component';
import { DetailItemCourseComponent } from './detail-items-course/detail-item-course.component';
import { EditCourseComponent } from './edit-course/edit-course.component';

const routes: Routes = [
  {
    path: '',
    component: DashboardDefaultComponent,
  }, {
    path: 'detail/:id',
    component: DetailCourseComponent,
  }, {
    path: 'item/:id',
    component: DetailItemCourseComponent,
  }, {
    path: 'edit/:id',
    component: EditCourseComponent,
  }, {
    path: 'create',
    component: CreateCourseComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CourseRoutingModule { }
