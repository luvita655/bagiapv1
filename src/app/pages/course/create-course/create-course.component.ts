import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core'
import { CourseService } from '../../../services/course.service'
import { RsPaginationModel } from '../../../model/rs-pagination.model'
import { CourseModel } from '../../../model/course.model'
import { PaginationModel } from '../../../model/pagination.model'
import { ItemsCourseModel } from '../../../model/items-course.model'
import { PassDataService } from '../../../services/pass-data.services'
import { DetailCourseModel, validateDetailCourse } from '../../../model/detail-course.model'
import { distinctUntilChanged, map, pluck, switchMap, tap, startWith, withLatestFrom } from 'rxjs/operators'
import { Subscription, forkJoin, pipe, Observable, Subject } from 'rxjs'
import { ActivatedRoute, Router } from '@angular/router'
import { ToastrService } from 'ngx-toastr'
import { CustomToastComponent } from '../../../widget-custom/custom-toast.component'
import { ZoneModel } from '../../../model/zone.model'
import { FormControl } from '@angular/forms'

@Component({
  selector: 'app-create-course',
  templateUrl: './create-course.component.html',
  styleUrls: ['./create-course.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CreateCourseComponent implements OnInit {
  
  page: number = 1
  per_page: number = 10
  error: string = null
  detailCourse: DetailCourseModel
  detailCourse$: Observable<DetailCourseModel>;
  loading: boolean = false;
  zones: ZoneModel[];
  lstWidth: string[];
  formData: any;
  file: File;
  urls: any[];
  layoutTypeItem: string;
  zoneItem: ZoneModel;
  zoneId: number = null;
  layoutType: string = null;
  lstZone: ZoneModel[];

  // rxjs 
  private readonly $itemCreateCourse = new Subject()
  createCourse: DetailCourseModel;
  // zone 
  private readonly $itemZoneDetail = new Subject()
  itemZoneEdit$: Observable<ZoneModel[]>;
  courseId: number = null;
  imageString: string = null;
  detailCourseItem: DetailCourseModel;
  stateDis: FormControl;
  stateName: FormControl;

  constructor (
    private _course: CourseService,
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private router: Router) {

      // init data
      this.loading = true;
      this.lstWidth = ['list_full_width', 'list_half_width', 'list_free_width', 'list_free_width_separate',
            'grid_half_width', 'grid_third_width']
      this.stateDis = new FormControl();
      this.stateName = new FormControl();
      this.validDetailCourse = new validateDetailCourse();
      this.lstZone = [];
      this.createCourse = new DetailCourseModel();
      this.layoutTypeItem = this.lstWidth[0];

      // set data
      this.itemZoneEdit$ = this.$itemCreateCourse.pipe(
        startWith(null),
        switchMap( _ => {
          return this._course.getAllZone();
        }),
        map(rs => {
          this.loading = false;
          const lstZone = rs.result.data.zones;
          this.zoneItem = lstZone[0];
          return rs.result.data.zones;
        }),
      )
  }

  ngOnInit () {
    //
  }
  
  onSelectFile(event) {
    let fileList: FileList = event.target.files;
    if(fileList) {
      this.file = fileList[0];
    }
  }

  cancelEdit() {
    this.router.navigate(['course']).then();
  }

  validDetailCourse: validateDetailCourse;
  validData(dataItem: any): boolean {
  this.validDetailCourse = new validateDetailCourse();

  const name = dataItem['name'] ? String(dataItem['name']).trim() : null;
  if(!name) {
    this.validDetailCourse.detect_name = true;
    this.validDetailCourse.smg_name = 'Vui lòng nhập dữ liệu!';
    return false;
  }
  const description = dataItem['description'] ? String(dataItem['description']).trim() : null;
  if(!description) {
    this.validDetailCourse.detect_description = true;
    this.validDetailCourse.smg_description = 'Vui lòng nhập dữ liệu!';
    return false;
  }
  const banner = dataItem && dataItem['banner'] ? String(dataItem['banner']).trim() : null;
  if(!banner) {
    this.validDetailCourse.detect_banner = true;
    this.validDetailCourse.smg_banner = 'Vui lòng nhập dữ liệu!';
    return false;
  }

    return true;
  }

  createCourseFunc() {
    const dataItem = {
      name: this.stateName.value,
      description: this.stateDis.value,
      layout_type: this.layoutTypeItem,
      zone_id: this.zoneItem ? this.zoneItem.id : this.zoneId,
    }
    if (this.file) {
      dataItem['banner'] = this.file 
    }
    const valid = this.validData(dataItem);
    if (valid) {
      this.loading = true;
      this._course.createNewCourse(dataItem).subscribe(res => {
        if(res.result.code === 200) {
          this.router.navigate(['course']).then();
          this.toastSuccess(res.result.message);
          this.loading = false;
        } else {
          this.toastErr('lỗi');
          this.loading = false;
        }
      }, err => {
        this.loading = false;
      });
    }
  }

  onChangeType(event) {
    this.layoutTypeItem = event;
  }

  onChangeZone(event) {
    const id = Number(event);
    this.zoneItem = this.lstZone.find(rs => rs.id === id);
  }

  toastSuccess(name: string){
    this.toastr.success(name,'', {toastComponent: CustomToastComponent});
  }

  toastErr(name: string){
    this.toastr.error(name,'', {toastComponent: CustomToastComponent});
  }
}
