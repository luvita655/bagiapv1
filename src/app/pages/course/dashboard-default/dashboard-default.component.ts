import { Component, OnInit, ViewChild } from '@angular/core';
import { CourseService } from '../../../services/course.service';
import { RsPaginationModel } from '../../../model/rs-pagination.model';
import { CourseModel } from '../../../model/course.model';
import { PaginationModel } from '../../../model/pagination.model';
import { ItemsCourseModel } from '../../../model/items-course.model';
import { DetailCourseModel } from '../../../model/detail-course.model';
import { PassDataService } from '../../../services/pass-data.services';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { CustomToastComponent } from '../../../widget-custom/custom-toast.component';
import { Observable, Subject } from 'rxjs';
import { TabsetComponent, TabDirective } from 'ngx-bootstrap/tabs';
import { DiscoverCourseModel } from '../../../model/discover-course.model';
import { finalize, switchMap, tap, withLatestFrom } from 'rxjs/operators';
import {startWith} from 'rxjs/operators/startWith';
import {map} from 'rxjs/operators/map';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-dashboard-default',
  templateUrl: './dashboard-default.component.html',
  styleUrls: [
    './dashboard-default.component.scss'
  ]
})
export class DashboardDefaultComponent implements OnInit {

  @ViewChild('tabset', {static: true}) tabset: TabsetComponent;
  @ViewChild('first', {static: true}) first: TabDirective;
  @ViewChild('second', {static: true}) second: TabDirective;


  page: number = 1;
  per_page: number = 10;

  lstCourse: CourseModel[];
  pagination: PaginationModel;
  discoverPag: PaginationModel;
  lstItemCourse: ItemsCourseModel[];
  error: string = null;
  detailCourse: DetailCourseModel;
  lstDiscoverCourse: DiscoverCourseModel[];
  ckChangeName: boolean = true;
  inxCourse: number = null;
  bkItemCourse: CourseModel;
  loading: boolean = false;
  observador: Observable<any>;
  stateNameCourse: FormControl;

  private readonly $itemCourseChanged = new Subject()
  courseItem$: Observable<CourseModel>;

  private readonly $itemCourseDisChanged = new Subject()
  courseDisItem$: Observable<CourseModel>;

  selectedtag: boolean = false;

  constructor(
    private _course: CourseService,
    private router: Router,
    private toastr: ToastrService,
    private _passData: PassDataService,) {
    this.lstCourse = [];
    this.pagination = new PaginationModel();
    this.discoverPag = new PaginationModel();
    this.lstItemCourse = [];
    this.lstDiscoverCourse = [];
    this.detailCourse = new DetailCourseModel();
    this.loading = false;
    this.stateNameCourse = new FormControl();

    // rxjs
    this.loading = true;
    this.courseItem$ = this.$itemCourseChanged.pipe(
      startWith(null),
      withLatestFrom(rs => this.stateNameCourse.valueChanges),
      switchMap((rs: any) => {
        if (!this.stateNameCourse.value || String(this.stateNameCourse.value).trim().length === 0) {
          return this._course.getListCourseByKey('', this.page, this.per_page);
        } else {
          return this._course.getListCourseByKey(this.stateNameCourse.value, this.page, this.per_page);
        }
      }),
      map(rs => {
        this.loading = false;
        return rs.result.data;
      })
    )

    this.courseDisItem$ = this.$itemCourseDisChanged.pipe(
      withLatestFrom(rs => this.stateNameCourse.valueChanges),
      switchMap((rs: any) => {
        if (!this.stateNameCourse.value || String(this.stateNameCourse.value).trim().length === 0) {
          return this._course.getDiscoverCourse('', this.page, this.per_page);
        } else {
          return this._course.getDiscoverCourse(this.stateNameCourse.value, this.page, this.per_page);
        }
      }),
      map(rs => {
        this.loading = false;
        return rs.result.data;
      })
    )
  }

  searchItem(event) {
    this.loading = true;
    if (!this.selectedtag) {
      this.$itemCourseChanged.next();
    } else {
      this.$itemCourseDisChanged.next();
    }
  }

  ngOnInit() {
    //
  }

  onPageChangeDis(event) {
    this.page = event;
    // this.getDiscoverCourse('', this.page, this.per_page);
    this.loading = true;
    this.$itemCourseDisChanged.next();
  }

  delCourse(course_id: string) {
    this.loading = true;
    const courseId: any = {'course_id': String(course_id)};
    this._course.delCourse(courseId).subscribe(res => {
      if (res.result.code === 200) {
        this.$itemCourseChanged.next();
        this.toastSuccess('Xóa thành công');
      } else {
        this.toastErr(res.result.message);
      }
      this.loading = false;
    }, err => {
      this.loading = false;
    });
  }

  showItemByCourse(course_id: string) {
    this.router.navigate(['course/item/', course_id]).then();
  }

  editExcercise(course_id: string) {
    this.router.navigate(['course/edit/', course_id]).then();
  }

  createItem() {
    this.router.navigate(['course/create']).then();
  }

  doubName(inx: number, item: CourseModel) {
    this.inxCourse = inx;
    this.ckChangeName = false;
    const itemCourse = this.lstCourse[inx];
    this.backupData(itemCourse);
  }

  resetItemCourse() {
    this.lstCourse[this.inxCourse] = this.bkItemCourse;
    this.inxCourse = null;
    this.ckChangeName = false;
  }

  backupData(item: CourseModel) {
    this.bkItemCourse = new CourseModel();
    this.bkItemCourse.id = item.id;
    this.bkItemCourse.name = item.name;
    this.bkItemCourse.banner = item.banner;
  }

  validateName(item: CourseModel): boolean {
    if (!item.name.trim()) {
      this.error = "nhập name"
      this.ckError = true;
      return false;
    } 

    this.error = ""
    this.ckError = false;
    return true;
  }

  ckError: boolean = false;
  saveEdit(item: CourseModel) {
    const valid = this.validateName(item);
    if (valid) {
        
    }
  }

  // TODO: Use Resolver
  getDetailCourse(course_id: string) {
    this.router.navigate(['/course/detail', course_id]).then();
  }

  toastSuccess(name: string){
    this.toastr.success(name,'', {toastComponent: CustomToastComponent});
  }

  toastErr(name: string){
    this.toastr.error(name,'', {toastComponent: CustomToastComponent});
  }

  onPageChange(event) {
    this.page = event;
    this.loading = true;
    this.$itemCourseChanged.next();
  }

  tabSelect(id: number) {
    this.loading = true;
    if (id === 0) {
      this.$itemCourseChanged.next();
      this.loading = true;
    } else if (id === 1) {
      this.$itemCourseDisChanged.next();
      this.loading = true;
    }
  }

}