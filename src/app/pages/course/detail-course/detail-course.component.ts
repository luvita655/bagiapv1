import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core'
import { CourseService } from '../../../services/course.service'
import { RsPaginationModel } from '../../../model/rs-pagination.model'
import { CourseModel } from '../../../model/course.model'
import { PaginationModel } from '../../../model/pagination.model'
import { ItemsCourseModel } from '../../../model/items-course.model'
import { PassDataService } from '../../../services/pass-data.services'
import { DetailCourseModel } from '../../../model/detail-course.model'
import { distinctUntilChanged, map, pluck, switchMap, tap } from 'rxjs/operators'
import { Subscription, forkJoin, pipe, Observable } from 'rxjs'
import { ActivatedRoute, Router } from '@angular/router'
import { ToastrService } from 'ngx-toastr'
import { CustomToastComponent } from '../../../widget-custom/custom-toast.component'

@Component({
  selector: 'app-detail-course',
  templateUrl: './detail-course.component.html',
  styleUrls: ['./detail-course.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DetailCourseComponent implements OnInit {
  page: number = 1
  per_page: number = 10
  error: string = null
  detailCourse: DetailCourseModel
  detailCourse$: Observable<DetailCourseModel>;
  private routeSub: Subscription;
  private currentCourseId: string;
  loading: boolean = false;

  constructor (
    private _course: CourseService,
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private router: Router) {
      this.loading = false;
  }

  ngOnInit () {
    this.loading = true;
    this.detailCourse$ = this.route.params.pipe(
      pluck('id'),
      switchMap(course_id => this._course.getDetailCourse({course_id: course_id})),
      map((res: any) => {
        this.toastSuccess(res.message);
        this.loading = false;
        return res.result.data;
      }),
    );

      // this.route.params.pipe(
      //   pluck('idForApi1'),
      //   switchMap(idApi1 => this.api1(idApi1)),
      //   switchMap(resultApi1 => forkJoin([
      //     of(resultApi1),
      //     this.api2(resultApi1)
      //   ])),
      //   map(([resultAPi1, resultApi2]) => {})
      // )


    // this.route.queryParams.pipe(
    //   switchMap(filter => forkJoin([
    //     this.api1(filter.filter1),
    //     this.api2(filter.filter2)
    //   ])),
    //   map(([apiResult1, apiResult2]) => {})
    // )
  }

  // nextCourse() {
  //   this.router.navigate(['/course/detail', Number(this.currentCourseId) + 1])
  // }

  // prevCourse() {
  //   this.router.navigate(['/course/detail', Number(this.currentCourseId) - 1])
  // }

  ngOnDestroy() {
    // this.routeSub.unsubscribe();
  }

  toastSuccess(name: string){
    this.toastr.success(name,'', {toastComponent: CustomToastComponent});
  }

  toastErr(name: string){
    this.toastr.error(name,'', {toastComponent: CustomToastComponent});
  }
}
