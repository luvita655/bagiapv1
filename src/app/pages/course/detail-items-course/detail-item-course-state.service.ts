import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { ItemsCourseModel } from '../../../model/items-course.model';

@Injectable()
export class DetailItemCourseStateService {
    private readonly $detailItemCourse = new BehaviorSubject<ItemsCourseModel>(null);
    readonly detailItemCourse$ = this.$detailItemCourse.asObservable();

    setItems(items:ItemsCourseModel) {
        this.$detailItemCourse.next(items);
    }

    removeItem(itemId: number) {
        const currentItem = this.$detailItemCourse.getValue();
        this.$detailItemCourse.next({...currentItem,items: currentItem.items.filter(item => item.id !== itemId)});
    }

    removeItemGroup(idGroup: number) {
        const currentItem = this.$detailItemCourse.getValue();
        const items = currentItem.items.filter(item => {
            item.group_workouts =  item.group_workouts.filter(rsGroup => rsGroup.group_id !== idGroup);
            return item;
        });
        this.$detailItemCourse.next({...currentItem,items: items});
    }
}