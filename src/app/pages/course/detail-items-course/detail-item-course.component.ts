import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  TemplateRef,
  ViewChild,
  OnDestroy
} from '@angular/core'
import { ItemsCourseModel } from '../../../model/items-course.model'
import { PassDataService } from '../../../services/pass-data.services'
import { Subscription, Observable, Subject } from 'rxjs'
import { ActivatedRoute } from '@angular/router'
import {
  finalize,
  map,
  pluck,
  startWith,
  switchMap,
  take,
  takeUntil,
  tap,
  withLatestFrom
} from 'rxjs/operators'
import { CourseService } from '../../../services/course.service'
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop'
import { ExerciseModel } from '../../../model/exercises.model'
import { ItemExerciseModel } from '../../../model/item-exercise.model'
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal'
import { MatDialog } from '@angular/material/dialog'
import { detailItemDialogComponent } from './detail-item-dialog/detail-item-dialog.component'
import { ItemService } from '../../../services/item.service'
import { DetailItemsCourseModel } from '../../../model/detail-item-course.model'
import * as _ from 'lodash'
import { FormBuilder, FormControl, FormGroup } from '@angular/forms'
import { merge } from 'rxjs-compat/operator/merge'
import { DetailItemCourseStateService } from './detail-item-course-state.service'
import { GroupService } from '../../../services/group.service'
import { ToastrService } from 'ngx-toastr'
import { CustomToastComponent } from '../../../widget-custom/custom-toast.component'
import { NewGroupDialogComponent } from './new-group-dialog/new-group-dialog.component'
import { ConfirmDialogComponent } from '../../../widget-custom/confirm-dialog/confirm-dialog.component'

@Component({
  selector: 'app-detail-item-course',
  templateUrl: './detail-item-course.component.html',
  styleUrls: ['./detail-item-course.component.scss'],
  providers: [DetailItemCourseStateService]
})
export class DetailItemCourseComponent implements OnInit, OnDestroy {
  page: number = 1;
  per_page: number = 10;
  error: string = null;
  detailItemCourse1: ItemsCourseModel;
  private routeSub: Subscription;
  isCollapsed = {};
  detailItemCourse$: Observable<ItemsCourseModel> = this.detailItemCourseStateService.detailItemCourse$;
  lstExercise: ExerciseModel;
  modalRef: BsModalRef;
  loading: boolean = false;
  loadingItemGroup: boolean = false;
  lstExcercises: ItemExerciseModel[];
  course_id: number = null;
  inxItemCourse: number = null;
  ckError: boolean = false;
  courseItemForm: FormGroup;
  createItemCourse: boolean = false;
  courseName: string = null;
  stateCourseName: FormControl;
  private readonly $destroy = new Subject();
  stateNewGroup: FormControl;
  indexGroup: number = null;

  // rxjs 
  private readonly $itemCourseDetail = new Subject()
  itemCourse$: Observable<ItemsCourseModel>;
  courseId: number = null;
  detectName: boolean = false;
  smgName: string = null;

  constructor (
    private cd: ChangeDetectorRef,
    private _course: CourseService,
    private _group: GroupService,
    private route: ActivatedRoute,
    private _passData: PassDataService,
    private modalService: BsModalService,
    private _itemService: ItemService,
    private cdr: ChangeDetectorRef,
    private fb: FormBuilder,
    private dialog: MatDialog,
    private toastr: ToastrService,
    private detailItemCourseStateService: DetailItemCourseStateService) {

      // init 
      this.loading = true;
      this.stateCourseName = new FormControl();
      this.stateNewGroup = new FormControl();

      // rxjs
      this.itemCourse$ = this.$itemCourseDetail.pipe(
        startWith(null),
        withLatestFrom(this.route.params.pipe(pluck('id'))),
        switchMap(([_, idRoute]) => {
          this.courseId = idRoute;
          return this._course.showItemByCourse({course_id: idRoute});
        }),
        map(rs => {
          this.loading = false;
          return rs.result.data;
        }),
      )
  }

  ngOnInit () {
    this.courseItemForm = this.fb.group({
      editCourseItem: []
    })

    // this.loading = true;
    // this.route.params.pipe(
    //   pluck('id'),
    //   switchMap(course_id => {
    //     this.course_id = course_id;
    //     return this._course.showItemByCourse({course_id});
    //   }),
    //   map((res: any) => {
    //     return res.result.data;
    //   }),
    //   takeUntil(this.$destroy) // unsubscribe - prevent memory leak
    // ).subscribe(itemCourseModel => {
    //   this.loading = false;
    //   this.detailItemCourseStateService.setItems(itemCourseModel); // set initial items after fetch from API
    // });
  }

  ngOnDestroy () {
    // this.$destroy.next();
    // this.$destroy.complete();
  }

  getDetailGroupWorkout(group_id: number) {
    this.showModalWithId(group_id);
  }

  showModalWithId (group_id: number) {
    this.dialog
      .open(detailItemDialogComponent, {
        data: group_id,
        disableClose: true
      })
      .afterClosed()
      .subscribe(res => {
        if (res) {
          console.log(res)
        } else {
          this.cdr.detectChanges()
        }
      })
  }

  showModal (lstItem: ExerciseModel) {
    this.dialog
      .open(detailItemDialogComponent, {
        data: lstItem,
        disableClose: true
      })
      .afterClosed()
      .subscribe(res => {
        if (res) {
          console.log(res)
        } else {
          this.cdr.detectChanges()
        }
      })
  }

  editItemCourse (inx: number, item: DetailItemsCourseModel) {
    this.inxItemCourse = inx
    this.bkEditNameItem(item)
  }

  saveEdit (dataItem: DetailItemsCourseModel) {
    this.loading = true
    this._itemService.updBasicItem(dataItem.id, dataItem.name).subscribe(
      res => {
        if (res.result.code) {
          this.courseItemForm.controls.editCourseItem.setValue(dataItem.name);
          this.inxItemCourse = null;
          this.bkDetailItemsCourse = new DetailItemsCourseModel();
          this.toastSuccess(res.result.message);
        } else {
          this.toastErr(res.result.message);
        }
        this.loading = false
      },
      err => {
        this.loading = false
      }
    )
  }

  bkDetailItemsCourse: DetailItemsCourseModel
  bkEditNameItem (item: DetailItemsCourseModel) {
    this.bkDetailItemsCourse = new DetailItemsCourseModel()
    this.bkDetailItemsCourse.name = item.name
    this.bkDetailItemsCourse.id = item.id
  }

  resetItemCourse () {
    this.inxItemCourse = null
    this.courseItemForm.controls.editCourseItem.setValue(
      this.bkDetailItemsCourse.name
    )
    this.bkDetailItemsCourse = new DetailItemsCourseModel()
  }

  delItemGroup (dataItem: DetailItemsCourseModel, group_item_id: number) {
    const confirmDialog = this.dialog.open(ConfirmDialogComponent, {
      data: null,
    });
    confirmDialog.afterClosed().subscribe(result => {
      if (result) {
        this.loading = true;
        this._group.delGroupItem(group_item_id)
            .subscribe(rs => {
              if (rs.result.code === 200) {
                this.$itemCourseDetail.next()
                this.toastSuccess(rs.result.message);
              } else {
                this.toastErr(rs.result.message);
              }
              this.loading = false;
            });
      }
    });
  }

  delAllGroup (dataItem: DetailItemsCourseModel) {
    const confirmDialog = this.dialog.open(ConfirmDialogComponent, {
      data: null,
    });
    confirmDialog.afterClosed().subscribe(result => {
      if (result) {
        this.loading = true;
        this._itemService
          .delGroupItem(dataItem.id)
          .subscribe(res => {
            if (res.result.code === 200) {
              this.$itemCourseDetail.next()
              this.toastSuccess(res.result.message);
            } else {
              this.toastErr(res.result.message);
            }
            this.loading = false;
          });
      }
    });
  }

  createItem () {
    this.createItemCourse = true;
  }

  cancelData() {
    this.clear();
    this.createItemCourse = false;
    this.stateCourseName.setValue(null);
  }

  validCourseName(): boolean {
    this.clear();
    const name = this.stateCourseName.value ? String(this.stateCourseName.value).trim() : null;
    
    if(!name) {
      this.detectName = true;
      this.smgName = 'chưa nhập';
      return false;
    }

    return true;
  }

  clear() {
    this.detectName = false;
    this.smgName = null;
  }

  // saveItem() {
  //   this.loading = true;
  //   const valid = this.validCourseName();
  //   if (valid) {
  //     this._course
  //       .createNewCourse(this.courseName, this.courseId )
  //       .subscribe(
  //       res => {
  //         if (res.result.code === 200) {
  //           this.$itemCourseDetail.next();
  //           this.stateCourseName.setValue(null);
  //           this.createItemCourse = false;
  //           this.toastSuccess(res.result.message);
  //         } else {
  //           this.toastErr(' thêm mới thất bại');
  //         }
  //         this.loading = false;
  //       });
  //   } else {
  //     this.loading = false;
  //   }
  // }

  addnewExcercise () {
    this.loading = true
  }

  toastSuccess(name: string){
    this.toastr.success(name,'', {toastComponent: CustomToastComponent});
  }

  toastErr(name: string){
    this.toastr.error(name,'', {toastComponent: CustomToastComponent});
  }

  createGroup(groupId: any) {
    this.dialog
    .open(NewGroupDialogComponent, {
      data: groupId.id,
      disableClose: true
    })
    .afterClosed()
    .subscribe(res => {
      this.$itemCourseDetail.next();
    })
  }
}
