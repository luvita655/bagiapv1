import { Component, OnInit, Input, TemplateRef, Inject, Renderer2, OnDestroy } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ItemExerciseModel } from '../../../../model/item-exercise.model';
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';
import { FormControl } from '@angular/forms';
import { from, Observable, Subject} from 'rxjs';
import {startWith} from 'rxjs/operators/startWith';
import {map} from 'rxjs/operators/map';
import { ExcerciseService } from '../../../../services/exercise.service';
import { ExerciseModel } from '../../../../model/exercises.model';
import { GroupService } from '../../../../services/group.service';
import { defaultIfEmpty, filter, finalize, flatMap, mergeMap, switchMap, take, tap, withLatestFrom } from 'rxjs/operators';
import { CourseService } from '../../../../services/course.service';
import { ToastrService } from 'ngx-toastr';
import { CustomToastComponent } from '../../../../widget-custom/custom-toast.component';

@Component({
  selector: 'detail-item-dialog',
  templateUrl: './detail-item-dialog.component.html',
  styleUrls: ['./detail-item-dialog.component.scss']
})
export class detailItemDialogComponent implements OnInit, OnDestroy {

  lstExercise: ExerciseModel;
  dataItemGroup: ItemExerciseModel[];
  loadingItem: boolean = false;
  stateCtrl: FormControl;
  lstSearchExcercise: ItemExerciseModel[];
  lstSttItemGroup: any[];
  itemKeyWord: string = null;
  detectAdd: boolean = false;
  searchKey: string = null;

  filteredStates: Observable<ItemExerciseModel[]>;
  private readonly $itemCourseChanged = new Subject()


  private readonly $itemExcerciseChanged = new Subject()
  excerciseItem$: Observable<ExerciseModel>;
  groupId: number = null;
  selectedExcerciseItem$: Observable<boolean>;

  constructor(
    @Inject(MAT_DIALOG_DATA) private data: number,
    private dialogRef: MatDialogRef<detailItemDialogComponent>,
    private _excercise: ExcerciseService,
    private _course: CourseService,
    private toastr: ToastrService,
    private _group: GroupService,) {

      // init
      this.lstSttItemGroup = [];
      this.loadingItem = true;
      this.lstSearchExcercise = [];
      this.stateCtrl = new FormControl();

      // get init data
      if (this.data) {
        this.loadingItem = true;
        this.groupId = this.data;
        this.excerciseItem$ = this.$itemExcerciseChanged.pipe(
          startWith(null),
          switchMap(() => this._course.showDetailGroupWorkout(this.groupId)),
          map(rs => {
            this.lstExercise = rs.result.data;
            this.lstSttItemGroup = this.lstExercise.exercises.map((res, inx) => {
              const item = {index: inx, id: res.id}
              return item;
            });
            this.loadingItem = false;
            return rs.result.data.exercises;
          }),
        )
      }

      this.filteredStates = this.$itemCourseChanged.pipe(
        tap(res => this.loadingItem = true),
        startWith(null),
        withLatestFrom(this.stateCtrl.valueChanges),
        switchMap(([_, keyword]) => this._excercise.getExcerciseByKeyword(keyword, 1, 10)),
        map(rs => {
          this.loadingItem = false;
          return rs.result.data.exercises;
        }),
      )
    }

    getDetailGroupWorkout (group_id: any) {
      this.dataItemGroup = []
      this.loadingItem = true;
      this._course.showDetailGroupWorkout(group_id).subscribe(
        res => {
          this.lstExercise = res.result.data;
          this.dataItemGroup = this.lstExercise.exercises;
          this.loadingItem = false;
        },
        err => {}
      )
    }

  ngOnDestroy() {
    
  }

  ngOnInit() {
    //
  }

  drop(event: CdkDragDrop<ItemExerciseModel[]>) {
    moveItemInArray(this.dataItemGroup, event.previousIndex, event.currentIndex);
    this.moveToLocation(event.previousIndex, event.currentIndex);
  }

  saveChange() {
    this.loadingItem = true;
    this._excercise.updExcercise(this.lstExercise.id, this.lstSttItemGroup).subscribe(res => {
      if (res.result.code === 200) {
        this.$itemExcerciseChanged.next();
        this.toastSuccess('Thành công');
      } else {
        this.toastSuccess('Thất bại');
      }
      this.loadingItem = false;
    })
  }

  moveToLocation(start: number, end: number) {
    const middle = this.lstSttItemGroup[start];
    this.lstSttItemGroup[start] = this.lstSttItemGroup[end];
    this.lstSttItemGroup[end] = middle;
  }

  addNewExcercise() {
    this.detectAdd = !this.detectAdd;
  }


  onEnter(evt: any, data: ItemExerciseModel){
    if (evt.source.selected) {
      this.loadingItem = true;
      this._excercise.addNewExcerciseToGroup(this.lstExercise.id, data.id).subscribe(res => {
        if (res.result.code === 200){
          this.stateCtrl.setValue(null);
          this.$itemExcerciseChanged.next();
          this.toastSuccess('Thành công');
        } else {
          this.toastSuccess('Thất bại');
        }
        this.loadingItem = false;
      }, err => {
        this.toastSuccess('Thất bại');
        this.loadingItem = false;
      });
    }
  }

  searchItem($event) {
    this.$itemCourseChanged.next();
  }

  delExcerciseOfGroup(exercise_group_id: number, indexItem: number) {
    this.loadingItem = true;
    this._group.delExcerciseOfGroup(exercise_group_id)
    .subscribe(res => {
        if (res.result.code === 200){
          this.$itemExcerciseChanged.next();
          this.toastSuccess('Thành công');
        } else {
          this.toastSuccess('Thất bại');
        }
        this.loadingItem = false;
    })
  }

  toastSuccess(name: string){
    this.toastr.success(name,'', {toastComponent: CustomToastComponent});
  }

  toastErr(name: string){
    this.toastr.error(name,'', {toastComponent: CustomToastComponent});
  }
}