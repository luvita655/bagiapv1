import { Component, OnInit, Input, TemplateRef, Inject, Renderer2, OnDestroy } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ItemExerciseModel } from '../../../../model/item-exercise.model';
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';
import { FormControl } from '@angular/forms';
import { Observable, Subject} from 'rxjs';
import {startWith} from 'rxjs/operators/startWith';
import {map} from 'rxjs/operators/map';
import { ExcerciseService } from '../../../../services/exercise.service';
import { ExerciseModel } from '../../../../model/exercises.model';
import { GroupService } from '../../../../services/group.service';
import { finalize, switchMap, tap, withLatestFrom } from 'rxjs/operators';
import { CourseService } from '../../../../services/course.service';
import { ToastrService } from 'ngx-toastr';
import { CustomToastComponent } from '../../../../widget-custom/custom-toast.component';
import { PaginationModel } from '../../../../model/pagination.model';

@Component({
  selector: 'detail-item-dialog',
  templateUrl: './new-group-dialog.component.html',
  styleUrls: ['./new-group-dialog.component.scss']
})
export class NewGroupDialogComponent implements OnInit, OnDestroy {

  lstExercise: ExerciseModel;
  dataItemGroup: ItemExerciseModel[];
  loadingItem: boolean = false;
  stateCtrl: FormControl;
  lstSearchExcercise: ItemExerciseModel[];
  lstSttItemGroup: any[];
  itemKeyWord: string = null;
  detectAdd: boolean = false;
  searchKey: string = null;

  filteredStates: Observable<ItemExerciseModel[]>;
  private readonly $itemCourseChanged = new Subject()
  searchGroup: FormControl;


  private readonly $itemGroupChanged = new Subject()
  groupItem$: Observable<ExerciseModel>;
  groupId: number = null;
  page: number = null;
  per_page: number = null;
  itemId: number = null;
  detectOrder: boolean = false;
  smgOrder: string = null;

  constructor(
    @Inject(MAT_DIALOG_DATA) private data: number,
    private dialogRef: MatDialogRef<NewGroupDialogComponent>,
    private _excercise: ExcerciseService,
    private _course: CourseService,
    private toastr: ToastrService,
    private _group: GroupService,) {

      // init
      this.lstSttItemGroup = [];
      this.loadingItem = true;
      this.lstSearchExcercise = [];
      this.stateCtrl = new FormControl();
      this.page = 1;
      this.per_page = 10;
      this.detectOrder = false;
      this.smgOrder = null;
      this.searchGroup = new FormControl();

      // get init data
      if (this.data) {
        this.itemId = this.data;
        this.loadingItem = true;
        this.groupId = this.data;
        this.groupItem$ = this.$itemGroupChanged.pipe(
          startWith(null),
          withLatestFrom(rs => this.searchGroup.valueChanges),
          switchMap((rs: any) => {
            if (!this.searchGroup.value || String(this.searchGroup.value).trim().length === 0) {
              return this._group.getGroupByKeyword('', this.page, this.per_page)
            } else {
              return this._group.getGroupByKeyword(this.searchGroup.value, this.page, this.per_page)
            }
          }),
          map(rs => {
            this.loadingItem = false;
            return rs.result.data;
          })
        )
      }
    }

    searchItem($event) {
      this.loadingItem = true;
      this.$itemGroupChanged.next();
    }

    getDetailGroupWorkout (group_id: any) {
      this.dataItemGroup = []
      this.loadingItem = true;
      this._course.showDetailGroupWorkout(group_id).subscribe(
        res => {
          this.lstExercise = res.result.data;
          this.dataItemGroup = this.lstExercise.exercises;
          this.loadingItem = false;
        },
        err => {}
      )
    }

  ngOnDestroy() {
    
  }

  ngOnInit() {
    //
  }

  toastSuccess(name: string){
    this.toastr.success(name,'', {toastComponent: CustomToastComponent});
  }

  toastErr(name: string){
    this.toastr.error(name,'', {toastComponent: CustomToastComponent});
  }

  onPageChangeDis(event) {
    this.page = event;
    this.loadingItem = true;
    this.$itemGroupChanged.next();
  }

  clean() {
    this.stateCtrl.setValue(null);
  }

  validateOders() {
    this.detectOrder = false;
    this.smgOrder = null;
    const order = this.stateCtrl.value ? String(this.stateCtrl.value).trim() : null;
    if (!order) {
      this.detectOrder = true;
      this.smgOrder = "chưa chọn!";

      return false;
    }

    return true;
  }

  actAddNewGroup(itemGroup: any) {
    this._group.addNewGroup(this.itemId, itemGroup.id, this.stateCtrl.value).subscribe(res => {
      if (res.result.code === 200) {
        this.toastSuccess(res.result.message);
      } else {
        this.toastErr('lỗi');
      }
    })
  }
}