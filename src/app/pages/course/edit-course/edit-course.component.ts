import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core'
import { CourseService } from '../../../services/course.service'
import { RsPaginationModel } from '../../../model/rs-pagination.model'
import { CourseModel } from '../../../model/course.model'
import { PaginationModel } from '../../../model/pagination.model'
import { ItemsCourseModel } from '../../../model/items-course.model'
import { PassDataService } from '../../../services/pass-data.services'
import { DetailCourseModel, validateDetailCourse } from '../../../model/detail-course.model'
import { distinctUntilChanged, map, pluck, switchMap, tap, startWith, withLatestFrom } from 'rxjs/operators'
import { Subscription, forkJoin, pipe, Observable, Subject } from 'rxjs'
import { ActivatedRoute, Router } from '@angular/router'
import { ToastrService } from 'ngx-toastr'
import { CustomToastComponent } from '../../../widget-custom/custom-toast.component'
import { ZoneModel } from '../../../model/zone.model'
import { FormControl } from '@angular/forms'

@Component({
  selector: 'app-edit-course',
  templateUrl: './edit-course.component.html',
  styleUrls: ['./edit-course.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditCourseComponent implements OnInit {
  
  page: number = 1
  per_page: number = 10
  error: string = null
  detailCourse: DetailCourseModel
  detailCourse$: Observable<DetailCourseModel>;
  loading: boolean = false;
  zones: ZoneModel[];
  lstWidth: string[];
  formData: any;
  file: File;
  urls: any[];
  layoutTypeItem: string;
  zoneItem: ZoneModel;
  zoneId: number = null;
  layoutType: string = null;
  lstZone: ZoneModel[];

  // rxjs 
  private readonly $itemCourseEditDetail = new Subject()
  itemCourseEdit$: Observable<DetailCourseModel>;
  // zone 
  private readonly $itemZoneDetail = new Subject()
  itemZoneEdit$: Observable<ZoneModel[]>;
  courseId: number = null;
  imageString: string = null;
  detailCourseItem: DetailCourseModel;
  stateDis: FormControl;
  stateName: FormControl;

  constructor (
    private _course: CourseService,
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private router: Router) {

      // init data
      this.loading = true;
      this.lstWidth = ['list_full_width', 'list_half_width', 'list_free_width', 'list_free_width_separate',
            'grid_half_width', 'grid_third_width']
      this.stateDis = new FormControl();
      this.stateName = new FormControl();
      this.validDetailCourse = new validateDetailCourse();
      this.lstZone = [];

      // set data
      this.itemCourseEdit$ = this.$itemCourseEditDetail.pipe(
        startWith(null),
        withLatestFrom(this.route.params.pipe(pluck('id'))),
        switchMap(([_, idRoute]) => {
          this.courseId = idRoute;
          return this._course.getDetailCourse({course_id: idRoute});
        }),
        map(rs => {
          this.loading = false;
          this.detailCourseItem = rs.result.data
          this.imageString = this.detailCourseItem.banner;
          this.zoneId = this.detailCourseItem.zone_id;
          
          this.layoutTypeItem = this.detailCourseItem.layout_type;
          return rs.result.data;
        }),
      )

      // set data
      this.itemZoneEdit$ = this.$itemZoneDetail.pipe(
        startWith(null),
        switchMap(() => {
          return this._course.getAllZone();
        }),
        map(rs => {
          this.loading = false;
          this.lstZone = rs.result.data.zones;
          return this.lstZone;
        }),
      )
  }

  ngOnInit () {
    //
  }
  
  onSelectFile(event) {
    let fileList: FileList = event.target.files;
    if(fileList) {
      this.file = fileList[0];
    }
  }

  cancelEdit() {
    this.router.navigate(['course']).then();
  }

  validDetailCourse: validateDetailCourse;
  validData(dataItem: any): boolean {
  this.validDetailCourse = new validateDetailCourse();

  const name = dataItem['name'] ? String(dataItem['name']).trim() : null;
  if(!name) {
    this.validDetailCourse.detect_name = true;
    this.validDetailCourse.smg_name = 'Vui lòng nhập dữ liệu!';
    return false;
  }
  const description = dataItem['description'] ? String(dataItem['description']).trim() : null;
  if(!description) {
    this.validDetailCourse.detect_description = true;
    this.validDetailCourse.smg_description = 'Vui lòng nhập dữ liệu!';
    return false;
  }
  // const banner = dataItem['banner'] ? String(dataItem['banner']).trim() : null;
  // if(!banner) {
  //   this.validDetailCourse.detect_banner = true;
  //   this.validDetailCourse.smg_banner = 'Vui lòng nhập dữ liệu!';
  //   return false;
  // }

    return true;
  }

  updExcerciseDetail() {

    const dataItem = {
      course_id: this.courseId,
      name: this.stateName.value,
      description: this.stateDis.value,
      layout_type: this.layoutTypeItem,
      zone_id: this.zoneItem ? this.zoneItem.id : this.zoneId,
    }

    if (this.file) {
      dataItem['banner'] = this.file;
    }
    const valid = this.validData(dataItem);
    if (valid) {
      this.loading = true;
      this._course.updCourse(dataItem).subscribe(res => {
        if(res.result.code === 200) {
          this.router.navigate(['course']).then();
          this.toastSuccess(res.result.message);
        } else {
          this.toastErr('lỗi');
        }

        this.loading = false;
      });
    }
  }

  onChangeType(event) {
    this.layoutTypeItem = event;
  }

  onChangeZone(event) {
    const id = Number(event);
    this.zoneItem = this.lstZone.find(rs => rs.id === id);
  }

  toastSuccess(name: string){
    this.toastr.success(name,'', {toastComponent: CustomToastComponent});
  }

  toastErr(name: string){
    this.toastr.error(name,'', {toastComponent: CustomToastComponent});
  }
}
