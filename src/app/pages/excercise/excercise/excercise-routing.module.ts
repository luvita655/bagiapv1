import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateExcerciseComponent } from './excercise/create/create-excercise.component';
import { EditExcerciseComponent } from './excercise/edit/edit-excercise.component';
import {ExcerciseComponent} from './excercise/excercise.component';
import { ShowDetailExcerciseComponent } from './excercise/show-detail/show-detail-excercise.component';

const routes: Routes = [
  {
    path: '',
    component: ExcerciseComponent,
  }, {
    path: 'detail/:id',
    component: ShowDetailExcerciseComponent,
  // }, {
  //   path: 'edit/:id',
  //   component: EditExcerciseComponent,
  }, {
    path: 'create',
    component: CreateExcerciseComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExcerciseRoutingModule { }
