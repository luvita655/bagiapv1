import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ExcerciseRoutingModule } from './excercise-routing.module';
import { ExcerciseComponent } from './excercise/excercise.component';
import {SharedModule} from '../../../shared/shared.module';
import {ToastyModule} from 'ng2-toasty';
import { ShowDetailExcerciseComponent } from './excercise/show-detail/show-detail-excercise.component';
import { EditExcerciseComponent } from './excercise/edit/edit-excercise.component';
import { CreateExcerciseComponent } from './excercise/create/create-excercise.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { WidgetCustomModule } from '../../../widget-custom/widget-custom.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { DemoMaterialModule } from '../../../material-module';
import { ExcerciseService } from '../../../services/exercise.service';
import { HttpService } from '../../../services/common/http.service';
import { GroupService } from '../../../services/group.service';
import { MatTooltipModule } from '@angular/material';
import { MaxLengthDirective } from '../../../widget-custom/input-max-length.directive';

@NgModule({
  imports: [
    CommonModule,
    ExcerciseRoutingModule,
    SharedModule,
    WidgetCustomModule,
    ReactiveFormsModule,
    FontAwesomeModule,
    DemoMaterialModule,
    FormsModule,
    ToastyModule.forRoot(),
    MatTooltipModule
  ],
  declarations: [
    ExcerciseComponent,
    ShowDetailExcerciseComponent,
    EditExcerciseComponent,
    CreateExcerciseComponent,
    MaxLengthDirective
  ],
  providers: [
     ExcerciseService,
     HttpService,
     GroupService
  ],
  entryComponents: [
    EditExcerciseComponent,
  ],
})
export class ExcerciseModule { }
