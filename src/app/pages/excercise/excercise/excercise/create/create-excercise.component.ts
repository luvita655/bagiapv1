import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import { ItemExerciseModel, validate } from '../../../../../model/item-exercise.model';
import * as _ from 'lodash'
import { FormBuilder, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ExcerciseService } from '../../../../../services/exercise.service';
import { ToastrService } from 'ngx-toastr';
import { CustomToastComponent } from '../../../../../widget-custom/custom-toast.component';

@Component({
  selector: 'app-create-excercise',
  templateUrl: './create-excercise.component.html',
  styleUrls: [
    './create-excercise.component.scss',
    '../../common.scss',
  ],
  encapsulation: ViewEncapsulation.None
})
export class CreateExcerciseComponent implements OnInit {

  itemExcercise: ItemExerciseModel;
  urls: any[];
  urlsThumb: any[];
  lstTTGguide: any[];
  newTTS: boolean = true;

  stateTTGguide: FormControl;
  excerciseId: number = null;
  loading: boolean = false;
  itemType: string = null;
  lstType: any[] = ['by_time', 'by_rep'];

  constructor(
    private route: ActivatedRoute,
    private _excercise: ExcerciseService,
    private formBuilder: FormBuilder,
    private fb: FormBuilder,
    private toastr: ToastrService,
    private router: Router,) {
    // init
    this.itemExcercise = new ItemExerciseModel();
    this.itemExcercise.valid = new validate();
    this.loading = false;
    this.lstTTGguide = [];
    this.stateTTGguide = new FormControl();
    this.urls = [];
    this.urlsThumb = [];
  }

  ngOnInit() {
    //
  }
  
  formData: any;
  file: File;
  onSelectFile(event) {
    let fileList: FileList = event.target.files;
    if(fileList) {
      this.file = fileList[0];
    }
  }
  
  formDataThumb: any;
  fileThumb: File;
  onSelectFileThumb(event) {
    let fileListThumb: FileList = event.target.files;
    if(fileListThumb) {
      this.fileThumb = fileListThumb[0];
    }
  }

  addNewTTS() {
    this.newTTS = false;
  }

  cancelAdd() {
    this.newTTS = true;
  }

  delTTGguide(inx: number) {
    _.remove(this.lstTTGguide, rs => rs && rs.id === inx);
  }

  actAddNewTTS() {
    if (String(this.stateTTGguide.value).trim()) {
      const key = String(this.stateTTGguide.value).length > 10 ? String(this.stateTTGguide.value).substr(0, 10) : String(this.stateTTGguide.value);
      const newTTGguide = {id: this.lstTTGguide.length, name: this.stateTTGguide.value, thumb: key}
      this.lstTTGguide.push(newTTGguide);
      this.stateTTGguide.setValue("");
    }
  }

  clean() {
    this.stateTTGguide.setValue("");
  }

  onChangeType(changeItem: any) {
    this.itemType = changeItem;
  }

  validate(dataValid: any): boolean {
    this.itemExcercise.valid = new validate();
    const name = dataValid['name'] ? String(dataValid['name']).trim() : null;
    if(!name) {
      this.itemExcercise.valid.detect_name = true;
      this.itemExcercise.valid.smg_name = 'Vui lòng nhập dữ liệu!';
      return false;
    }
    // file
    if(!this.file) {
      this.itemExcercise.valid.detect_image= true;
      this.itemExcercise.valid.smg_image = 'Vui lòng nhập dữ liệu!';
      return false;
    }
    // file thumb
    if(!this.fileThumb) {
      this.itemExcercise.valid.detect_image_thumb= true;
      this.itemExcercise.valid.smg_image_thumb = 'Vui lòng nhập dữ liệu!';
      return false;
    }
    // description
    const dis = dataValid['description'] ? String(dataValid['description']).trim() : null;
    if(!dis) {
      this.itemExcercise.valid.detect_description= true;
      this.itemExcercise.valid.smg_description = 'Vui lòng nhập dữ liệu!';
      return false;
    }
    // video
    const video = dataValid['video'] ? String(dataValid['video']).trim() : null;
    if(!video) {
      this.itemExcercise.valid.detect_video= true;
      this.itemExcercise.valid.smg_video = 'Vui lòng nhập dữ liệu!';
      return false;
    }
    //type
    const type = dataValid['type'] ? String(dataValid['type']).trim() : null;
    if(!type) {
      this.itemExcercise.valid.detect_type= true;
      this.itemExcercise.valid.smg_type = 'Vui lòng nhập dữ liệu!';
      return false;
    }
    //reps
    const reps = dataValid['reps'] ? String(dataValid['reps']).trim() : null;
    if(!reps) {
      this.itemExcercise.valid.detect_reps= true;
      this.itemExcercise.valid.smg_reps = 'Vui lòng nhập dữ liệu!';
      return false;
    }
    //TimePerRep
    const TimePerRep = dataValid['time_per_rep'] ? String(dataValid['time_per_rep']).trim() : null;
    if(!TimePerRep) {
      this.itemExcercise.valid.detect_time_per_rep= true;
      this.itemExcercise.valid.smg_time_per_rep = 'Vui lòng nhập dữ liệu!';
      return false;
    }
    //TTGguideData
    if (!dataValid['tts_guide']) {
      this.itemExcercise.valid.detect_tts_guide= true;
      this.itemExcercise.valid.smg_tts_guide = 'Vui lòng nhập dữ liệu!';
      return false;
    }
    //met
    const met = dataValid['met'] ? String(dataValid['met']).trim() : null;
    if(!met) {
      this.itemExcercise.valid.detect_met= true;
      this.itemExcercise.valid.smg_met = 'Vui lòng nhập dữ liệu!';
      return false;
    }
    //stateDefaultDuration
    const stateDefaultDuration = dataValid['default_duration'] ? String(dataValid['default_duration']).trim() : null;
    if(!stateDefaultDuration) {
      this.itemExcercise.valid.detect_default_duration= true;
      this.itemExcercise.valid.smg_default_duration = 'Vui lòng nhập dữ liệu!';
      return false;
    }
    return true;
  }
  
  createExcercise() {
    let TTGguideData: string = '';
    this.lstTTGguide.map((res, inx) => {
      if (res) {
        TTGguideData.trim();
        TTGguideData += inx === this.lstTTGguide.length-1 ? res.name : res.name + ';';
      }
    });
    
    const dataItem = {
      name: this.itemExcercise.name,
      image: this.file,
      thumb_image: this.fileThumb,
      description: this.itemExcercise.description,
      video: this.itemExcercise.video,
      type: this.itemType,
      reps: this.itemExcercise.reps,
      time_per_rep: this.itemExcercise.time_per_rep,
      tts_guide: TTGguideData,
      met: this.itemExcercise.met,
      default_duration: this.itemExcercise.default_duration,
    }
    const valid = this.validate(dataItem);
    if (valid) {
      this.loading = true;
      this._excercise.createExcercise(dataItem).subscribe(res => {
        if (res.result.code === 200) {
          this.router.navigate(['excercise']).then();
          this.toastSuccess(res.result.message);
          this.itemExcercise.valid = new validate();
        } else {
          this.toastErr('Lỗi');
        }
        this.loading = false;
      });
    }
  }

  cancelEdit() {
    this.router.navigate(['excercise']).then();
  }

  toastSuccess(name: string){
    this.toastr.success(name,'', {toastComponent: CustomToastComponent});
  }

  toastErr(name: string){
    this.toastr.error(name,'', {toastComponent: CustomToastComponent});
  }
  
}
