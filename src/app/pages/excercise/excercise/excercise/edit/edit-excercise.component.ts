import {Component, Inject, OnInit, ViewEncapsulation} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {ToastData, ToastOptions, ToastyService} from 'ng2-toasty';
import { Observable, Subject } from 'rxjs';
import { map, pluck, startWith, switchMap, withLatestFrom } from 'rxjs/operators';
import { ExcerciseService } from '../../../../../services/exercise.service';
import { ItemExerciseModel, validate } from '../../../../../model/item-exercise.model';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import * as _ from 'lodash'
import { ToastrService } from 'ngx-toastr';
import { CustomToastComponent } from '../../../../../widget-custom/custom-toast.component';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-edit-excercise',
  templateUrl: './edit-excercise.component.html',
  styleUrls: [
    './edit-excercise.component.scss',
    '../../common.scss',
    '../excercise.component.scss'
  ],
  encapsulation: ViewEncapsulation.None
})
export class EditExcerciseComponent implements OnInit {
  
  loading: boolean = false;
  excerciseForm: FormGroup;
  name = 'Angular 4';
  urls: any[];
  lstTTGguide: any[];
  newTTS: boolean = true;
  lstType: any[] = ['by_time', 'by_rep'];
  itemType: string = null;

  stateTTGguide: FormControl;
  stateDefaultDuration: FormControl;
  stateMet: FormControl;
  stateVideo: FormControl;
  stateTimePerRep: FormControl;
  stateReps: FormControl;
  stateDis: FormControl;
  stateName: FormControl;
  excerciseId: number = null;
  imageString: string = null;
  // rxjs 
  private readonly $itemExcerciseDetail = new Subject()
  itemExcercise$: Observable<ItemExerciseModel>;
  
  constructor(
    @Inject(MAT_DIALOG_DATA) private data: number,
    private dialogRef: MatDialogRef<EditExcerciseComponent>,
    private route: ActivatedRoute,
    private _excercise: ExcerciseService,
    private formBuilder: FormBuilder,
    private fb: FormBuilder,
    private toastr: ToastrService,
    private router: Router,) {

      // init
    this.loading = true;
    this.lstTTGguide = [];
    this.stateTTGguide = new FormControl();
    this.stateDefaultDuration = new FormControl();
    this.stateMet = new FormControl();
    this.stateVideo = new FormControl();
    this.stateTimePerRep = new FormControl();
    this.stateReps = new FormControl();
    this.stateDis = new FormControl();
    this.stateName = new FormControl();
    this.urls = [];
    this.validData = new validate();

    // set data
    if (this.data) {
      this.excerciseId = this.data;
      this.itemExcercise$ = this.$itemExcerciseDetail.pipe(
        startWith(null),
        switchMap(() => this._excercise.getDetailExcercise(this.excerciseId)),
        map(rs => {
          this.loading = false;
          const data = rs.result.data;
          this.itemType = data.type;
          const arrTTG: string[] = _.split(data.tts_guide, ';');
          this.imageString = data.image;
          this.stateMet.setValue(data.met);
          this.lstTTGguide = arrTTG
                                .map((rs, inx) => 
                                  { 
                                    if(rs.trim()) {
                                      const key = rs.trim().length > 10 ? rs.trim().substr(0, 20) : rs.trim();
                                      const lstData = {id: inx, name: rs, thumb: key};
                                      return lstData;
                                    }
                                  });
          return rs.result.data;
        }
        ),
      )
    }
  }

  ngOnInit() {
    // $('[data-toggle="tooltip"]').tooltip();
  }

  ngAfterViewInit() {
    // $('[data-toggle="tooltip"]').tooltip();
  }

  addNewTTS() {
    this.newTTS = false;
  }

  cancelAdd() {
    this.newTTS = true;
  }

  delTTGguide(inx: number) {
    _.remove(this.lstTTGguide, rs => rs && rs.id === inx);
  }

  actAddNewTTS() {
    if (String(this.stateTTGguide.value).trim()) {
      const key = String(this.stateTTGguide.value).length > 10 ? String(this.stateTTGguide.value).substr(0, 10) : String(this.stateTTGguide.value);
      const newTTGguide = {id: this.lstTTGguide.length, name: this.stateTTGguide.value, thumb: key}
      this.lstTTGguide.push(newTTGguide);
      this.stateTTGguide.setValue("");
    }
  }

  clean() {
    this.stateTTGguide.setValue("");
  }
  
  formData: any;
  file: File;
  onSelectFile(event) {
    let fileList: FileList = event.target.files;
    if(fileList) {
      this.urls.push(fileList[0]);
      this.file = fileList[0];
    }
  }

  validData: validate;
  validate(dataValid: any): boolean {
    this.validData = new validate();
    const name = dataValid['name'] ? String(dataValid['name']).trim() : null;
    if(!name) {
      this.validData.detect_name = true;
      this.validData.smg_name = 'Vui lòng nhập dữ liệu!';
      return false;
    }
    // file
    if(!dataValid['image']) {
      this.validData.detect_image= true;
      this.validData.smg_image = 'Vui lòng nhập dữ liệu!';
      return false;
    }
    // description
    const dis = dataValid['description'] ? String(dataValid['description']).trim() : null;
    if(!dis) {
      this.validData.detect_description= true;
      this.validData.smg_description = 'Vui lòng nhập dữ liệu!';
      return false;
    }
    // video
    const video = dataValid['video'] ? String(dataValid['video']).trim() : null;
    if(!video) {
      this.validData.detect_video= true;
      this.validData.smg_video = 'Vui lòng nhập dữ liệu!';
      return false;
    }
    //type
    const type = dataValid['type'] ? String(dataValid['type']).trim() : null;
    if(!type) {
      this.validData.detect_type= true;
      this.validData.smg_type = 'Vui lòng nhập dữ liệu!';
      return false;
    }
    //reps
    const reps = dataValid['reps'] ? String(dataValid['reps']).trim() : null;
    if(!reps) {
      this.validData.detect_reps= true;
      this.validData.smg_reps = 'Vui lòng nhập dữ liệu!';
      return false;
    }
    //TimePerRep
    const TimePerRep = dataValid['time_per_rep'] ? String(dataValid['time_per_rep']).trim() : null;
    if(!TimePerRep) {
      this.validData.detect_time_per_rep= true;
      this.validData.smg_time_per_rep = 'Vui lòng nhập dữ liệu!';
      return false;
    }
    //TTGguideData
    if (!dataValid['tts_guide']) {
      this.validData.detect_tts_guide= true;
      this.validData.smg_tts_guide = 'Vui lòng nhập dữ liệu!';
      return false;
    }
    //met
    const met = dataValid['met'] ? String(dataValid['met']).trim() : null;
    if(!met) {
      this.validData.detect_met= true;
      this.validData.smg_met = 'Vui lòng nhập dữ liệu!';
      return false;
    }
    //stateDefaultDuration
    const stateDefaultDuration = dataValid['default_duration'] ? String(dataValid['default_duration']).trim() : null;
    if(!stateDefaultDuration) {
      this.validData.detect_default_duration= true;
      this.validData.smg_default_duration = 'Vui lòng nhập dữ liệu!';
      return false;
    }
    return true;
  }

  // updExcerciseDetail() {
  //   let TTGguideData: string = '';
  //   this.lstTTGguide.map((res, inx) => {
  //     if (res) {
  //       TTGguideData.trim();
  //       TTGguideData += inx === this.lstTTGguide.length-1 ? res.name : res.name + ';';
  //     }
  //   });
  //   const detect = this.file ? true : false;
  //   const dataItem = {
  //     id: this.excerciseId,
  //     name: this.stateName.value,
  //     image: this.file ? this.file : this.imageString,
  //     description: this.stateDis.value,
  //     video: this.stateVideo.value,
  //     type: this.itemType,
  //     reps: this.stateReps.value,
  //     time_per_rep: this.stateTimePerRep.value,
  //     tts_guide: TTGguideData,
  //     met: this.stateMet.value,
  //     default_duration: this.stateDefaultDuration.value,
  //   }

  //   const valid = this.validate(dataItem);
  //   if (valid) {
  //     this.loading = true;
  //     this._excercise.updExcerciseDetail(dataItem, detect, true).subscribe(res => {
  //       if (res.result.code === 200) {
  //         this.$itemExcerciseDetail.next();
  //         this.router.navigate(['excercise']).then();
  //         this.toastSuccess(res.result.message);
  //         this.validData = new validate();
  //         this.dialogRef.close(true);
  //       } else {
  //         this.toastErr('Lỗi');
  //       }
  //       this.loading = false;
  //     })
  //   }
  // }

  onChangeType(changeItem: any) {
    this.itemType = changeItem;
  }

  toastSuccess(name: string){
    this.toastr.success(name,'', {toastComponent: CustomToastComponent});
  }

  toastErr(name: string){
    this.toastr.error(name,'', {toastComponent: CustomToastComponent});
  }
  
}
