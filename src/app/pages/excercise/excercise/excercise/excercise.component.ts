import {ChangeDetectorRef, Component, OnInit, ViewEncapsulation} from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { map, startWith, switchMap, tap, withLatestFrom } from 'rxjs/operators';
import { ExcerciseService } from '../../../../services/exercise.service';
import { ItemExerciseModel, validate } from '../../../../model/item-exercise.model';
import { FormControl } from '@angular/forms';
import { ExerciseModel } from '../../../../model/exercises.model';
import { ExcerciseWithPagination } from '../../../../model/excercise-with-pagination.model';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { CustomToastComponent } from '../../../../widget-custom/custom-toast.component';
import { GroupService } from '../../../../services/group.service';
import { MatDialog, MatDialogRef } from '@angular/material';
import { EditExcerciseComponent } from './edit/edit-excercise.component';
import * as _ from 'lodash'
import { ConfirmDialogComponent } from '../../../../widget-custom/confirm-dialog/confirm-dialog.component';

@Component({
  selector: 'app-excercise',
  templateUrl: './excercise.component.html',
  styleUrls: [
    './excercise.component.scss',
    '../common.scss',
  ],
  encapsulation: ViewEncapsulation.None
})
export class ExcerciseComponent implements OnInit {

  name = 'Angular 4';
  urls = [];
  urlsThumb = [];
  loading: boolean = false;
  page: number = null;
  per_page: number = null;
  searchKey: string = null;
  stateCtrl: FormControl;
  lstExercise: ItemExerciseModel[];
  validateEx: validate;
  stateName: FormControl;
  stateDis: FormControl;
  stateVideo: FormControl;
  stateReps: FormControl;
  stateTimePerRep: FormControl;
  stateMet: FormControl;
  stateDefaultDuration: FormControl;
  stateTTGguide: FormControl;
  lstTTGguide: any[];
  lstTTGguideBK: any[];
  lstType: any[] = ['by_time', 'by_rep'];
  excerciseId: number = null;
  imageString: string = null;
  imageStringThumb: string = null;
  validData: validate;

  // rxjs 
  private readonly $itemExcerciseChanged = new Subject()
  lstExcercise$: Observable<ExcerciseWithPagination>;

  constructor(
    private _excercise: ExcerciseService,
    private toastr: ToastrService,
    private _group: GroupService,
    private cdr: ChangeDetectorRef,
    private dialog: MatDialog,
    private router: Router,) {
      // init
      this.stateCtrl = new FormControl('');
      this.page = 1;
      this.per_page = 10;
      this.loading = true;
      this.urls = [];
      this.urlsThumb = [];
      this.lstTTGguide = [];
      this.lstTTGguideBK = [];
      
      this.stateDefaultDuration = new FormControl();
      this.stateMet = new FormControl();
      this.stateVideo = new FormControl();
      this.stateTimePerRep = new FormControl();
      this.stateReps = new FormControl();
      this.stateDis = new FormControl();
      this.stateName = new FormControl();
      this.stateTTGguide = new FormControl();
      this.validData = new validate();
      this.itemType = [];

      // rxjs
      this.lstExcercise$ = this.$itemExcerciseChanged.pipe(
        startWith(null),
        switchMap(() => {
          if (!this.stateCtrl.value || String(this.stateCtrl.value).trim().length === 0) {
            return this._excercise.getExcerciseByKeyword("", this.page, this.per_page);
          } else {
            return this._excercise.getExcerciseByKeyword(String(this.stateCtrl.value).trim(), this.page, this.per_page);
          }
        }),
        map(rs => {
          this.lstExercise = rs.result.data.exercises;
          this.lstTTGguide = this.lstExercise.map(rs => {
            const thumb = _.split(rs.tts_guide, ';');
            this.itemType.push(rs.type);
            return thumb.map((item, inx) => {
              const key = item.trim().length > 10 ? item.trim().substr(0, 20) : item.trim();
              return {name: item, thumb: key, id: inx};
            });
          });
          this.urls = this.lstExercise.map(rs =>{ const item=rs.image.split("/"); return item[item.length-1];});
          this.urlsThumb = this.lstExercise.map(rs => {
            if (rs.thumb_image) {
              const item=rs.thumb_image.split("/");
              return item[item.length-1];
            } else return null;
          });
          this.loading = false;
          return rs.result.data;
        }),
      )
    }

  searchItem($event) {
    this.loading = true;
    this.$itemExcerciseChanged.next();
  }

  ngOnInit() {
    //
  }

  delExcercise(id_excercise: number) {
    const confirmDialog = this.dialog.open(ConfirmDialogComponent, {
      data: null,
    });
    confirmDialog.afterClosed().subscribe(result => {
      if (result) {
        this.loading = true;
        this._excercise.delExcercise(id_excercise).subscribe(res => {
          if (res.result.code === 200) {
            this.$itemExcerciseChanged.next();
            this.toastSuccess(res.result.message);
          } else {
            this.toastErr('Xóa thất bại');
          }
          this.loading = false;
        })
      }
    });

  }

  showDetail(id_excercise: number) {
    this.router.navigate(['excercise/detail/', id_excercise]).then();
  }

  editExcercise(id_excercise: number) {
    this.dialog
    .open(EditExcerciseComponent, {
      data: id_excercise,
      disableClose: true
    })
    .afterClosed()
    .subscribe(res => {
      if (res) {
        this.$itemExcerciseChanged.next();
      } else {
        this.cdr.detectChanges()
      }
    })
  }

  addNewExcercise() {
    this.router.navigate(['excercise/create/']).then();
  }

  toastSuccess(name: string){
    this.toastr.success(name,'', {toastComponent: CustomToastComponent});
  }

  toastErr(name: string){
    this.toastr.error(name,'', {toastComponent: CustomToastComponent});
  }

  onPageChange(event) {
    this.page = event;
    this.loading = true;
    this.$itemExcerciseChanged.next();
  }

  editId: number = null;
  doubEditExcercise(inx: number) {
    this.cloneTTG(this.lstTTGguide[inx]);
    this.editId = inx;
  }

  cloneTTG(lst: any[]) {
    this.lstTTGguideBK = [];
    lst.map(rs => {
      this.lstTTGguideBK.push(rs);
    })
  }

  cancelEdit() {
    this.cloneTTG(this.lstTTGguide[this.editId]);
    this.editId = null;
  }

  itemType: string[] = null;
  onChangeType(changeItem: any) {
    this.itemType[this.editId] = changeItem;
  }
  
  formData: any;
  file: File;
  onSelectFile(event) {
    let fileList: FileList = event.target.files;
    if(fileList) {
      this.file = fileList[0];
    }
  }
  
  formDataThumb: any;
  fileThumb: File;
  onSelectFileThumb(event) {
    let fileListThumb: FileList = event.target.files;
    if(fileListThumb) {
      this.fileThumb = fileListThumb[0];
    }
  }

  delTTGguide(inx: number, index: number) {
    _.remove(this.lstTTGguideBK, rs => rs && rs.id === inx);
  }

  clean() {
    this.stateTTGguide.setValue("");
  }

  actAddNewTTS() {
    if (String(this.stateTTGguide.value).trim()) {
      const key = String(this.stateTTGguide.value).length > 10 ? String(this.stateTTGguide.value).substr(0, 10) : String(this.stateTTGguide.value);
      const newTTGguide = {name: this.stateTTGguide.value, thumb: key, id: this.lstTTGguideBK.length}
      this.lstTTGguideBK.push(newTTGguide);
      this.stateTTGguide.setValue("");
    }
  }
  
  validate(dataValid: any): boolean {
    this.validData = new validate();
    const name = dataValid['name'] ? String(dataValid['name']).trim() : null;
    if(!name) {
      this.validData.detect_name = true;
      this.validData.smg_name = 'Vui lòng nhập dữ liệu!';
      return false;
    }

    // description
    const dis = dataValid['description'] ? String(dataValid['description']).trim() : null;
    if(!dis) {
      this.validData.detect_description= true;
      this.validData.smg_description = 'Vui lòng nhập dữ liệu!';
      return false;
    }
    // video
    const video = dataValid['video'] ? String(dataValid['video']).trim() : null;
    if(!video) {
      this.validData.detect_video= true;
      this.validData.smg_video = 'Vui lòng nhập dữ liệu!';
      return false;
    }

    //reps
    const reps = dataValid['reps'] ? String(dataValid['reps']).trim() : null;
    if(!reps) {
      this.validData.detect_reps= true;
      this.validData.smg_reps = 'Vui lòng nhập dữ liệu!';
      return false;
    }
    //TimePerRep
    const TimePerRep = dataValid['time_per_rep'] ? String(dataValid['time_per_rep']).trim() : null;
    if(!TimePerRep) {
      this.validData.detect_time_per_rep= true;
      this.validData.smg_time_per_rep = 'Vui lòng nhập dữ liệu!';
      return false;
    }
    //TTGguideData
    if (!dataValid['tts_guide']) {
      this.validData.detect_tts_guide= true;
      this.validData.smg_tts_guide = 'Vui lòng nhập dữ liệu!';
      return false;
    }
    //met
    const met = dataValid['met'] ? String(dataValid['met']).trim() : null;
    if(!met) {
      this.validData.detect_met= true;
      this.validData.smg_met = 'Vui lòng nhập dữ liệu!';
      return false;
    }
    //stateDefaultDuration
    const stateDefaultDuration = dataValid['default_duration'] ? String(dataValid['default_duration']).trim() : null;
    if(!stateDefaultDuration) {
      this.validData.detect_default_duration= true;
      this.validData.smg_default_duration = 'Vui lòng nhập dữ liệu!';
      return false;
    }
    return true;
  }

  actionEdit(exId: number) {
    let TTGguideData: string = '';
    this.lstTTGguideBK.map((res, inx) => {
      if (res) {
        TTGguideData.trim();
        TTGguideData += inx === this.lstTTGguideBK.length-1 ? res.name : res.name + ';';
      }
    });

    const dataItem = {
      id: exId,
      name: this.stateName.value,
      description: this.stateDis.value,
      video: this.stateVideo.value,
      type: this.itemType[this.editId],
      reps: this.stateReps.value,
      time_per_rep: this.stateTimePerRep.value,
      tts_guide: TTGguideData,
      met: this.stateMet.value,
      default_duration: this.stateDefaultDuration.value,
    }
    if (this.file) {
      dataItem['image'] = this.file;
    }
    if (this.fileThumb) {
      dataItem['thumb_image'] = this.fileThumb;
    }

    const valid = this.validate(dataItem);
    if (valid) {
      this.loading = true;
      this._excercise.updExcerciseDetail(dataItem).subscribe(res => {
        if (res.result.code === 200) {
          this.$itemExcerciseChanged.next();
          this.router.navigate(['excercise']).then();
          this.toastSuccess(res.result.message);
          this.validData = new validate();
          this.lstTTGguideBK = []
          this.editId = null;
          this.stateTTGguide.setValue("");
        } else {
          this.toastErr('Lỗi');
          this.loading = false;
        }
      }, err => {
        this.toastErr('Lỗi');
        this.loading = false;
      })
    }
  }
}
