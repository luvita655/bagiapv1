import {ChangeDetectorRef, Component, OnInit, ViewEncapsulation} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {ToastData, ToastOptions, ToastyService} from 'ng2-toasty';
import { Observable, Subject } from 'rxjs';
import { map, pluck, startWith, switchMap, withLatestFrom } from 'rxjs/operators';
import { ExcerciseService } from '../../../../../services/exercise.service';
import { ExcerciseWithPagination } from '../../../../../model/excercise-with-pagination.model';
import { ItemExerciseModel } from '../../../../../model/item-exercise.model';
import { MatDialog } from '@angular/material';
import { EditExcerciseComponent } from '../edit/edit-excercise.component';

@Component({
  selector: 'app-show-detail-excercise',
  templateUrl: './show-detail-excercise.component.html',
  styleUrls: [
    './show-detail-excercise.component.scss',
    '../excercise.component.scss'
  ],
  encapsulation: ViewEncapsulation.None
})
export class ShowDetailExcerciseComponent implements OnInit {

  // rxjs 
  private readonly $itemExcerciseDetail = new Subject()
  itemExcercise$: Observable<ItemExerciseModel>;
  loading: boolean = false;
  excerciseId: number = null;

  constructor(
    private route: ActivatedRoute,
    private _excercise: ExcerciseService,
    private cdr: ChangeDetectorRef,
    private dialog: MatDialog,
    private router: Router,
    ) {
      this.loading = true;
      this.itemExcercise$ = this.$itemExcerciseDetail.pipe(
        startWith(null),
        withLatestFrom(this.route.params.pipe(pluck('id'))),
        switchMap(([_, idRoute]) => {
          this.excerciseId = idRoute;
          return this._excercise.getDetailExcercise(idRoute);
        }),
        map(rs => {
          this.loading = false;
          return rs.result.data;
        }
        ),
      )
    }

  ngOnInit() {

  }

  editItemExcercise() {
    this.dialog
    .open(EditExcerciseComponent, {
      data: this.excerciseId,
      disableClose: true
    })
    .afterClosed()
    .subscribe(res => {
      if (res) {
        this.router.navigate(['excercise']).then();
      } else {
        this.cdr.detectChanges()
      }
    })
  }
  
}
