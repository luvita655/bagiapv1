import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GroupDetailComponent } from './group/group-detail/group-detail.component';
import { GroupPanelComponent } from './group/group-panel.component';
import { GroupEditComponent } from './group/group-edit/group-edit.component';
import { GroupCreateComponent } from './group/group-create/group-create.component';

const routes: Routes = [
  {
    path: '',
    component: GroupPanelComponent,
  }, {
    path: 'detail/:id',
    component: GroupDetailComponent,
  }, {
    path: 'edit/:id',
    component: GroupEditComponent,
  }, {
    path: 'create',
    component: GroupCreateComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GroupRoutingModule { }
