import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GroupRoutingModule } from './group-routing.module';
import { GroupDetailComponent } from './group/group-detail/group-detail.component';
import { GroupPanelComponent } from './group/group-panel.component';
import { GroupEditComponent } from './group/group-edit/group-edit.component';
import { GroupCreateComponent } from './group/group-create/group-create.component';

import {SharedModule} from '../../shared/shared.module';
import {AgmCoreModule} from '@agm/core';
import { ExcerciseRoutingModule } from '../excercise/excercise/excercise-routing.module';
import { WidgetCustomModule } from '../../widget-custom/widget-custom.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DemoMaterialModule } from '../../material-module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ExcerciseService } from '../../services/exercise.service';
import { GroupService } from '../../services/group.service';
import { HttpService } from '../../services/common/http.service';
import { HttpModule } from '@angular/http';
import { ChartModule } from 'angular2-chartjs';
import { CourseService } from '../../services/course.service';

@NgModule({
  imports: [
    GroupRoutingModule,
    SharedModule,
    ChartModule,
    HttpModule,
    WidgetCustomModule,
    FontAwesomeModule,
    FormsModule,
    CommonModule,
    DemoMaterialModule,
    WidgetCustomModule,
    ReactiveFormsModule,
  ],
  declarations: [
    GroupDetailComponent,
    GroupPanelComponent,
    GroupEditComponent,
    GroupCreateComponent,
  ],
  providers: [
     ExcerciseService,
     GroupService,
     HttpService,
     CourseService,
  ],
})
export class GroupModule { }
