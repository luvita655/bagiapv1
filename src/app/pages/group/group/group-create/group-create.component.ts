import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { GroupService } from '../../../../services/group.service';
import { GroupWorkoutsModel, validateGroup } from '../../../../model/group-workouts.model';
import { ToastrService } from 'ngx-toastr';
import { CustomToastComponent } from '../../../../widget-custom/custom-toast.component';

@Component({
  selector: 'app-group-create',
  templateUrl: './group-create.component.html',
  styleUrls: [
    './group-create.component.scss',
    '../../group.component.scss',
  ]
})
export class GroupCreateComponent implements OnInit {

  itemGroup: GroupWorkoutsModel;
  urls: any[];
  loading: boolean = false;
  stateName: FormControl;
  stateDis: FormControl;

  constructor(
    private _group: GroupService,
    private toastr: ToastrService,
    private router: Router,
    ) {
    this.itemGroup = new GroupWorkoutsModel();
    this.itemGroup.validate = new validateGroup();
    this.loading = false;
    this.stateDis = new FormControl();
    this.stateName = new FormControl();
    this.urls = [];
  }

  ngOnInit() {
    //
  }
  
  formData: any;
  file: File;
  onSelectFile(event) {
    let fileList: FileList = event.target.files;
    if(fileList) {
      this.file = fileList[0];
    }
  }

  fileThumb: File;
  onSelectFileThumb(event) {
    let fileList: FileList = event.target.files;
    if(fileList) {
      this.fileThumb = fileList[0];
    }
  }


  backToHomeGroup() {
    this.router.navigate(['group/']).then();
  }

  createGroup() {
    const dataItem = {
      name: this.stateName.value,
      banner: this.file,
      thumb: this.fileThumb,
      description: this.stateDis.value,
    }
    const valid = this.validate(dataItem);
    if (valid) {
      this.loading = true;
      this._group.createNewGroup(dataItem).subscribe(res => {
        if (res.result.code === 200) {
          this.router.navigate(['group']).then();
          this.itemGroup = new GroupWorkoutsModel();
          this.itemGroup.validate = new validateGroup();
          this.toastSuccess(res.result.message);
        } else {
          this.toastErr('lỗi');
        }
        this.loading = false;
      }, err => {
        this.toastErr('lỗi');
        this.loading = false;
      })
    }
  }

  validate(dataItem: any): boolean {
    this.itemGroup.validate = new validateGroup();
    if(!this.stateName.value || String(this.stateName.value).trim().length === 0) {
      this.itemGroup.validate.detectSmgName = true;
      this.itemGroup.validate.smgName = "chưa chọn Name";
      return false;
    }
    
    if (!dataItem['banner']) {
      this.itemGroup.validate.detectSmgBanner = true;
      this.itemGroup.validate.smgBanner = "chưa chọn Banner";
      return false;
    }
    
    if (!dataItem['thumb']) {
      this.itemGroup.validate.detectSmgThumb = true;
      this.itemGroup.validate.smgThumb = "chưa chọn Thumb";
      return false;
    }

    if(!this.stateDis.value || String(this.stateDis.value).trim().length === 0) {
      this.itemGroup.validate.detectSmgDescription = true;
      this.itemGroup.validate.smgDescription = "chưa chọn Description";
      return false;
    }

    return true;
  }

  toastSuccess(name: string){
    this.toastr.success(name,'', {toastComponent: CustomToastComponent});
  }

  toastErr(name: string){
    this.toastr.error(name,'', {toastComponent: CustomToastComponent});
  }

}
