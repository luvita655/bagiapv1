import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { map, pluck, startWith, switchMap, withLatestFrom } from 'rxjs/operators';
import { ExerciseModel } from '../../../../model/exercises.model';
import { ItemExerciseModel } from '../../../../model/item-exercise.model';
import { CourseService } from '../../../../services/course.service';

@Component({
  selector: 'app-group-detail',
  templateUrl: './group-detail.component.html',
  styleUrls: [
    './group-detail.component.scss',
    '../../group.component.scss'
  ]
})
export class GroupDetailComponent implements OnInit {

  // rxjs 
  private readonly $itemGroupDetail = new Subject()
  itemGroup$: Observable<ExerciseModel>;
  loading: boolean = false;
  groupId: number = null;
  
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private _course: CourseService,) {
      this.loading = true;
      this.itemGroup$ = this.$itemGroupDetail.pipe(
        startWith(null),
        withLatestFrom(this.route.params.pipe(pluck('id'))),
        switchMap(([_, idRoute]) => {
          this.groupId = idRoute;
          return this._course.showDetailGroupWorkout(idRoute);
        }),
        map(rs => {
          this.loading = false;
          return rs.result.data;
        }
        ),
      )
    }

  ngOnInit() {
    //
  }

  editItemGroup(id_group: number) {
    this.router.navigate(['group/edit/', id_group]).then();
  }

}
