import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { map, pluck, startWith, switchMap, withLatestFrom } from 'rxjs/operators';
import { GroupService } from '../../../../services/group.service';
import { ExerciseModel } from '../../../../model/exercises.model';
import { CourseService } from '../../../../services/course.service';
import { validateGroup } from '../../../../model/group-workouts.model';
import { ToastrService } from 'ngx-toastr';
import { CustomToastComponent } from '../../../../widget-custom/custom-toast.component';
@Component({
  selector: 'app-group-edit',
  templateUrl: './group-edit.component.html',
  styleUrls: [
    './group-edit.component.scss',
    '../../group.component.scss'
  ]
})
export class GroupEditComponent implements OnInit {

  // rxjs 
  private readonly $itemGroupDetail = new Subject()
  itemGroup$: Observable<ExerciseModel>;
  loading: boolean = false;
  groupId: number = null;
  urls: any[];
  stateDis: FormControl;
  stateName: FormControl;
  canvas: any;
  imageBanner: string = null;
  imageThumb: string = null;
  validDataGroup: validateGroup;
  formData: any;
  file: File;
  fileThumb: File;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService,
    private _group: GroupService,
    private _course: CourseService,) {
      
      // init 
      this.loading = true;
      this.urls = [];
      this.stateDis = new FormControl();
      this.stateName = new FormControl();
      this.canvas = document.createElement('canvas')
      this.validDataGroup = new validateGroup();

      // rxjs
      this.itemGroup$ = this.$itemGroupDetail.pipe(
        startWith(null),
        withLatestFrom(this.route.params.pipe(pluck('id'))),
        switchMap(([_, idRoute]) => {
          this.groupId = idRoute;
          return this._course.showDetailGroupWorkout(idRoute);
        }),
        map(rs => {
          this.loading = false;
          this.imageBanner = rs.result.data.banner;
          this.imageThumb = rs.result.data.thumb;
          return rs.result.data;
        }),
      )
    }

  ngOnInit() {
    //
  }

  cancel() {
    this.router.navigate(['group']).then();
  }
  
  onSelectFile(event) {
    let fileList: FileList = event.target.files;
    if(fileList) {
      this.file = fileList[0];
    }
  }

  onSelectFileThumb(event) {
    let fileList: FileList = event.target.files;
    if(fileList) {
      this.fileThumb = fileList[0];
    }
  }

  validate(): boolean {
    this.validDataGroup = new validateGroup();
    if(!this.stateName.value || String(this.stateName.value).trim().length === 0) {
      this.validDataGroup.detectSmgName = true;
      this.validDataGroup.smgName = "chưa chọn Name";
      return false;
    }

    if(!this.stateDis.value || String(this.stateDis.value).trim().length === 0) {
      this.validDataGroup.detectSmgDescription = true;
      this.validDataGroup.smgDescription = "chưa chọn Description";
      return false;
    }

    return true;
  }

  updGroupDetail() {
    const dataItem = {
      id: this.groupId,
      name: this.stateName.value,
      description: this.stateDis.value,
    }
    let detectChangeBanner = false;
    if (this.file) {
      dataItem['banner'] = this.file;
      detectChangeBanner = true;
    }
    let detectChangeThumb = false;
    if (this.fileThumb) {
      dataItem['thumb'] = this.fileThumb;
      detectChangeThumb = true;
    }

    const valid = this.validate();
    if (valid) {
      this.loading = true;
      this._group.updGroupDetail(dataItem, detectChangeBanner, detectChangeThumb).subscribe(res => {
        if (res.result.code === 200) {
          this.toastSuccess(res.result.message);
          this.router.navigate(['group']).then();
        } else {
          this.toastErr('lỗi');
        }
        this.loading = false;
      })
    }
  }

  toastSuccess(name: string){
    this.toastr.success(name,'', {toastComponent: CustomToastComponent});
  }

  toastErr(name: string){
    this.toastr.error(name,'', {toastComponent: CustomToastComponent});
  }

}
