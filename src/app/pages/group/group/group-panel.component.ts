import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { map, startWith, switchMap } from 'rxjs/operators';
import { GroupService } from '../../../services/group.service';
import { GroupWorkoutsModel } from '../../../model/group-workouts.model';

@Component({
  selector: 'app-group-panel',
  templateUrl: './group-panel.component.html',
  styleUrls: [
    './group-panel.component.scss',
    '../group.component.scss',
  ]
})
export class GroupPanelComponent implements OnInit {

  loading: boolean = false;
  page: number = null;
  per_page: number = null;
  searchKey: string = null;
  stateCtrl: FormControl;
  lstExercise: GroupWorkoutsModel;
  // rxjs 
  private readonly $itemGroupChanged = new Subject()
  lstGroup$: Observable<GroupWorkoutsModel>;

  constructor(
    private _group: GroupService,
    private router: Router,) {

      // init
      this.stateCtrl = new FormControl('');
      this.page = 1;
      this.per_page = 10;
      this.loading = true;

      this.lstGroup$ = this.$itemGroupChanged.pipe(
        startWith(null),
        switchMap(() => {
          if (!this.stateCtrl.value || String(this.stateCtrl.value).trim().length === 0) {
            return this._group.getGroupByKeyword("", this.page, this.per_page);
          } else {
            return this._group.getGroupByKeyword(String(this.stateCtrl.value).trim(), this.page, this.per_page);
          }
        }),
        map(rs => {
          this.loading = false;
          this.lstExercise = rs.result.data;
          return rs.result.data;
        }),
      )
    }

  ngOnInit() {
    //
  }

  searchItem($event) {
    this.$itemGroupChanged.next();
  }

  showDetail(id: number) {
    this.router.navigate(['group/detail/', id]).then();
  }

  editGroup(id: number) {
    this.router.navigate(['group/edit/', id]).then();
  }

  delExcercise(id_group: number) {
    this.loading = true;
    this._group.delGroup(id_group).subscribe(res => {
      this.loading = false;
      this.$itemGroupChanged.next();
    });
  }

  addNewExcercise() {
    this.router.navigate(['group/create/']).then();
  }

  onPageChange(event) {
    this.page = event;
    this.loading = true;
    this.$itemGroupChanged.next();
  }

}
