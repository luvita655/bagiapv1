import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpService } from './common/http.service';
import { ApiService } from './common/api.service';
import { ApiApplication } from '../config/app.config';
import { RsAccountModel } from '../model/rs-account.model';
import { RsPaginationModel } from '../model/rs-pagination.model';

type Dictionary<TValue> = {[key: string]: TValue};

@Injectable()
export class CourseService extends ApiService {
    // private cachedCourses: Record<string, Observable<any>>;

    constructor(http: HttpService, _router: Router ) { 
        super(ApiApplication.course.getCourse, http, _router);
    }

    getLstCourse(data: RsPaginationModel) {
        return this.post(ApiApplication.course.getCourse, data);
    }

    delCourse(course_id: string) {
        return this.post(ApiApplication.course.delCourse, course_id);
    }

    showItemByCourse(course_id: any) {
        return this.post(ApiApplication.course.itemByCourse, course_id);
    }

    getDetailCourse(course_id: any) { 
        return this.post(ApiApplication.course.detailCourse, course_id);
    }

    showDetailGroupWorkout(group_id: any) {
        const dataItem = {group_id: group_id};
        return this.post(ApiApplication.course.detailGroupWorkout, dataItem);
    }

    createNewCourse(dataItem: any) {
        const formData: FormData = new FormData();
        formData.append("name", dataItem['name']);
        formData.append("banner", dataItem['banner']);
        formData.append("description", dataItem['description']);
        formData.append("layout_type", dataItem['layout_type']);
        formData.append("zone_id", dataItem['zone_id']);

        return this.postWithUpload(ApiApplication.course.createNewCourse, formData);
    }

    getAllZone() {
        return this.post(ApiApplication.course.getZone);
    }

    updCourse(dataItem: any) {
        const formData: FormData = new FormData();
        formData.append("course_id", dataItem['course_id']);
        formData.append("name", dataItem['name']);
        formData.append("description", dataItem['description']);
        formData.append("layout_type", dataItem['layout_type']);
        formData.append("zone_id", dataItem['zone_id']);

        if (dataItem['banner']) {
            formData.append("banner", dataItem['banner']);
        }

        return this.postWithUpload(ApiApplication.course.updBasicCourse, formData);
    }
    
    getDiscoverCourse(keyword: string, page: number, per_page: number) {
        const formData: any = new FormData();
        formData.append("keyword", keyword);
        formData.append("page", page);
        formData.append("per_page", per_page);
        return this.postWithUpload(ApiApplication.course.getDiscoverCourse, formData);
    }

    getListCourseByKey(keyword: string, page: number, per_page: number) {
        const formData: any = new FormData();
        formData.append("keyword", keyword);
        formData.append("page", page);
        formData.append("per_page", per_page);
        return this.postWithUpload(ApiApplication.course.getLstCourseByKey, formData);
    }
    
}
