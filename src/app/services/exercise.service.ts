import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpService } from './common/http.service';
import { ApiService } from './common/api.service';
import { ApiApplication } from '../config/app.config';

@Injectable()
export class ExcerciseService extends ApiService {

    constructor(http: HttpService, _router: Router ) { 
        super(ApiApplication.group.delGroupItem, http, _router);
    }

    getExcerciseByKeyword(keyword: string, page: number, per_page: number) {
        const dataItem = {keyword: keyword, page: page, per_page: per_page};
        return this.post(ApiApplication.exercise.exerciseByKeyword, dataItem);
    }

    updExcercise(group_id: number, exercise_order: any[]) {
        const dataItem = {group_id: group_id, exercise_order: exercise_order};
        return this.post(ApiApplication.exercise.updOrderExercise, dataItem);
    }

    addNewExcerciseToGroup(group_id: number, exercise_id: number) {
        const dataItem = {group_id: group_id, exercise_id: exercise_id};
        return this.post(ApiApplication.exercise.addNewExerciseToGroup, dataItem);
    }

    getDetailExcercise(exercise_id: number){
        const dataItem = {exercise_id: String(exercise_id)};
        return this.post(ApiApplication.exercise.getDetailExcercise, dataItem);
    }

    updExcerciseDetail(dataItem: any) {
        const formData: FormData = new FormData();
        formData.append("id", dataItem['id']);
        formData.append("name", dataItem['name']);
        if (dataItem && dataItem['image']) {
            formData.append("image", dataItem['image'], dataItem['image'].name);
        }
        if (dataItem && dataItem['thumb_image']) {
            formData.append("thumb_image", dataItem['thumb_image'], dataItem['thumb_image'].name);
        }
        formData.append("description", dataItem['description']);
        formData.append("video", dataItem['video']);
        formData.append("type", dataItem['type']);
        formData.append("reps", dataItem['reps']);
        formData.append("time_per_rep", dataItem['time_per_rep']);
        formData.append("tts_guide", dataItem['tts_guide']);
        formData.append("met", dataItem['met']);
        formData.append("default_duration", dataItem['default_duration']);

        return this.postWithUpload(ApiApplication.exercise.updExcercise, formData);
    }

    createExcercise(dataItem: any) {
        const formData: FormData = new FormData();
        formData.append("name", dataItem['name']);
        formData.append("image", dataItem['image'], dataItem['image'].name);
        formData.append("thumb_image", dataItem['thumb_image'], dataItem['thumb_image'].name);
        formData.append("description", dataItem['description']);
        formData.append("video", dataItem['video']);
        formData.append("type", dataItem['type']);
        formData.append("reps", dataItem['reps']);
        formData.append("time_per_rep", dataItem['time_per_rep']);
        formData.append("tts_guide", dataItem['tts_guide']);
        formData.append("met", dataItem['met']);
        formData.append("default_duration", dataItem['default_duration']);

        return this.postWithUpload(ApiApplication.exercise.createExcercise, formData);
    }

    delExcercise(dataItem: any) {
        const formData: FormData = new FormData();
        formData.append("exercise_id", dataItem);
        return this.postWithUpload(ApiApplication.exercise.delExcercise, formData);
    }
    
}