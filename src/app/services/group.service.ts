import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpService } from './common/http.service';
import { ApiService } from './common/api.service';
import { ApiApplication } from '../config/app.config';
import { RsAccountModel } from '../model/rs-account.model';

@Injectable()
export class GroupService extends ApiService {

    constructor(http: HttpService, _router: Router ) { 
        super(ApiApplication.group.delGroupItem, http, _router);
    }

    delGroupItem(group_item_id: number) {
        const groupOfItem = {group_item_id: group_item_id}
        return this.post(ApiApplication.group.delGroupItem, groupOfItem);
    }

    delExcerciseOfGroup(exercise_group_id: number) {
        const groupOfItem = {exercise_group_id: exercise_group_id}
        return this.post(ApiApplication.group.delExerciseOfGroup, groupOfItem);
    }

    getGroupByKeyword(keyword: string, page: number, per_page: number) {
        const groupByKey = {keyword: keyword, page: page, per_page: per_page}
        return this.post(ApiApplication.group.groupByKeyword, groupByKey);
    }

    createNewGroup(dataItem: any) {
        const formData: FormData = new FormData();
        formData.append("name", dataItem['name']);
        formData.append("banner", dataItem['banner'], dataItem['banner'].name);
        formData.append("description", dataItem['description']);

        return this.postWithUpload(ApiApplication.group.createGroup, formData);
    }

    delGroup(group_id: number) {
        const dataItem = {group_id: group_id};
        return this.post(ApiApplication.group.delGroup, dataItem);
    }

    updGroupDetail(dataItem: any, detectBanner: boolean, detectThumb: boolean) {
        const formData: FormData = new FormData();
        formData.append("id", dataItem['id']);
        if (detectBanner) {
            formData.append("banner", dataItem['banner'], dataItem['banner'].name);
        }
        if (detectThumb) {
            formData.append("thumb", dataItem['thumb'], dataItem['thumb'].name);
        }
        formData.append("name", dataItem['name']);
        formData.append("description", dataItem['description']);

        return this.postWithUpload(ApiApplication.group.updGroup, formData);
    }

    addNewGroup(item_id: number, group_id: number, order: number) {
        const dataItem = {item_id: item_id, group_id: group_id, order: order}
        return this.post(ApiApplication.group.addNewGroup, dataItem);
    }
    
}
