import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpService } from './common/http.service';
import { ApiService } from './common/api.service';
import { ApiApplication } from '../config/app.config';
import { RsAccountModel } from '../model/rs-account.model';
import { CourseService } from './course.service';
import { switchMap } from 'rxjs/operators';

@Injectable()
export class ItemService extends ApiService {

    constructor(
            http: HttpService,
            _router: Router,
            private _course: CourseService,
        ) { 
        super(ApiApplication.item.delItem, http, _router);
    }

    delGroupItem(item: any) {
        const dataItem = {item_id: item};
        return this.post(ApiApplication.item.delItem, dataItem);
    }

    updBasicItem(item_id: number, name: string) {
        const dataItem = {item_id: item_id, name: name};
        return this.post(ApiApplication.item.updBasicItem, dataItem);
    }
    
}
