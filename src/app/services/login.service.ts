import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpService } from './common/http.service';
import { ApiService } from './common/api.service';
import { ApiApplication } from '../config/app.config';
import { RsAccountModel } from '../model/rs-account.model';

@Injectable()
export class LoginService extends ApiService {
    
    username: string;
    password: string;

    constructor(http: HttpService, _router: Router ) { 
        super(ApiApplication.login, http, _router);
    }

    getLogin(login: RsAccountModel) {
        return this.postNoToken(ApiApplication.login, login);
    }

    // logout(logout: any) {
    //     return this.post(ApiApplication.logout, logout);
    // }
    
}
