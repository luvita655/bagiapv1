import { Injectable } from '@angular/core';
import { BehaviorSubject, ReplaySubject } from 'rxjs';
import { DetailCourseModel } from '../model/detail-course.model';
import { ItemsCourseModel } from '../model/items-course.model';

@Injectable()
export class PassDataService {
    
    constructor() {}
    
    private dataDetailCourse = new ReplaySubject<DetailCourseModel>(1);
    readonly dataDetailCourse$ = this.dataDetailCourse.asObservable();

    setDetailCourse(course: DetailCourseModel) {
        this.dataDetailCourse.next(course);
    }

    // getDetailCourse() {
    //     return this.dataDetailCourse.asObservable();
    // }
    
    private dataDetailItemCourse = new BehaviorSubject<ItemsCourseModel>(undefined);

    setDetailItemCourse(user: any) {
        this.dataDetailItemCourse.next(user);
    }

    getDetailItemCourse() {
        return this.dataDetailItemCourse.asObservable();
    }
  
}