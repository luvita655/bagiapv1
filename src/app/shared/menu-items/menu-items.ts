import {Injectable} from '@angular/core';

export interface BadgeItem {
  type: string;
  value: string;
}

export interface ChildrenItems {
  state: string;
  target?: boolean;
  name: string;
  type?: string;
  children?: ChildrenItems[];
}

export interface MainMenuItems {
  state: string;
  short_label?: string;
  main_state?: string;
  target?: boolean;
  name: string;
  type: string;
  icon: string;
  badge?: BadgeItem[];
  children?: ChildrenItems[];
}

export interface Menu {
  label: string;
  main: MainMenuItems[];
}

const MENUITEMS = [
  {
    label: 'Navigation',
    main: [
      {
        state: 'course',
        short_label: 'D',
        name: 'Course',
        type: 'link',
        icon: 'ti-home'
      },
      {
        state: 'excercise',
        short_label: 'n',
        name: 'Excercise',
        type: 'link',
        icon: 'ti-crown'
      },
      {
        state: 'group',
        short_label: 'n',
        name: 'Group',
        type: 'link',
        icon: 'ti-crown'
      },
    ],
  },
];

@Injectable()
export class MenuItems {
  getAll(): Menu[] {
    return MENUITEMS;
  }

  /*add(menu: Menu) {
    MENUITEMS.push(menu);
  }*/
}
