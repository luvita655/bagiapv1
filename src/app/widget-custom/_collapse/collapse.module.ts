import { NgModule, ModuleWithProviders } from '@angular/core';
import { CollapseDirective } from './collapse.directive';
import { HttpModule } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [CollapseDirective],
  exports: [CollapseDirective],
  imports: [
      HttpModule,
      CommonModule,
  ],
})
export class CollapseModule {
  static forRoot(): ModuleWithProviders {
    return { ngModule: CollapseModule, providers: [] };
  }
}