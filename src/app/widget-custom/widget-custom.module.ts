import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AlertComponent } from './_alert/alert.component';
import { HttpModule } from '@angular/http';
import { HttpService } from '../services/common/http.service';
import { AlertService } from './_alert/alert.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CollapseModule } from './_collapse/collapse.module';
import { CollapseDirective } from './_collapse/collapse.directive';
import { NgxLoadingComponent } from './ngx-loading.component';
import { DemoMaterialModule } from '../material-module';
import { CustomToastComponent } from './custom-toast.component';
import { ToastrModule } from 'ngx-toastr';
import { TooltipDirective } from './tooltip.directive';
import { ConfirmDialogComponent } from './confirm-dialog/confirm-dialog.component';
@NgModule({
    imports: [
        HttpModule,
        CommonModule,
        DemoMaterialModule,
        ToastrModule.forRoot()
    ],
    declarations: [
      AlertComponent,
      NgxLoadingComponent,
      CustomToastComponent,
      TooltipDirective,
      ConfirmDialogComponent,
    ],
    exports: [
      AlertComponent,
      NgxLoadingComponent,
    ],
    providers: [
      HttpService,
      AlertService,
    ],
    entryComponents: [
      CustomToastComponent,
      ConfirmDialogComponent,
    ],
})
export class WidgetCustomModule {
}